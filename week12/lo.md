# Week 12 Learning Objectives

# 1. I can explain the ALOHA and CSMA/CD protocols and how they solve the multiple-access problem.

The above protocols are known as random access protocols, which are ways of reducing and recovering from collisions in data transmission. There are two types of ALOHA protocol: Slotted ALOHA and Pure ALOHA. Slotted ALOHA uses time slots during which each node in a network can transmit a frame of data. Collision is detected when more than one node transmits during a designated time slot. In the event that a collision occurs, the sending node will attempt to retransmit the frame in a randomized manner until the frame is successfully transmitted. Some pros to Slotted ALOHA are that each node has access to the full channel while transmitting, the process is decentralized (specific to each node), and the protocol is simple. Some drawbacks are wasting slots when collision occurs, slots going unused, false collision detection, and clock synchronization. Due to its high fail rate and long processing time, the efficiency of Slotted ALOHA, at best, is only 37%. Pure ALOHA cuts that efficiency down to 18%; the only difference from Slotted ALOHA is a lack of synchronization amongst nodes; frames are sent over the channel as soon as they are received.

CSMA, or carrier sense multiple access, also has two different forms: one with collision detection and one without. Original CSMA transmits an entire data frame as soon as it detects the channel is not busy, waiting to transmit if the channel is busy. CSMA/CD can quickly detect collisions and abort colliding transmissions to unclog the channel. Once a transmission is aborted, CSMA/CD retransmits once it has gone through a binary backoff interval, choosing a random number between 0 and two to the number of collisions minus one to be a base risen to 512 to determine the bit time. This protocol is a huge improvement in efficiency from the ALOHA protocols because of its collision handling, upping the rate to 1.

# 2. I can compare and contrast various error correction and detection schemes, including parity bits, 2D parity, the Internet Checksum, and CRC.

A parity bit is a bit placed at the end of some data to determine whether or not any of the bits were flipped in transmission. It accomplishes this by ensuring even parity, meaning there must be an even number of ones in the binary. If there is an odd number in the raw data, the parity bit is a one to make it even; if there is already an even number of ones, the parity bit is a zero. Parity bits are great for checking for odd numbers of flipped bits, but the method falls apart when checking for even numbers of identical flipped bits; it could look as if there was no error.

2D parity is similar to single parity bit checking; it just adds another dimension to it, checking the parity of rows and columns in a data matrix. It also adds in the feature of error correction, so if an erroneous transmission is detected, it can be fixed so it is viable. 2D parity does not fully fix the problem with even numbers of identical bit flips, but they are more unlikely based on the matrix format rather than a straight line of binary.

An internet checksum tests for errors in every transmitted bit, not just the overall parity of data. The data is broken into 16-bit integer segments, the segments are added to find the one's complement, and the final sum is stored as the checksum. This computational process happens again on the receiver side of the transmission, and if the checksums do not match, there is definite error in the received data. There is always a chance of error regardless of whether or not it is detected, but the internet checksum is a more thorough examination than parity checking.

CRC, or cyclic redundancy check, is the best system of error checking within these options. It is essentially a form of long division where a specific bit pattern is divided into the data, and a certain number of bits are added to the end of the data so it is evenly divisible by the bit pattern. If the remainder between the bit pattern and data after transmission is not zero, error occurred. This method is able to check for errors up to one more than the number of bits added to the end of the data; more erroneous bits than that are unlikely.

# 3. I can describe the Ethernet protocol including how it implements each of the Layer 2 services and how different versions of Ethernet differ.

Ethernet is the most prevalent and first big local area network (LAN) protocol. It is simple, cheap, and versatile, using a single chip for multiple transmission speeds. There have been two versions of ethernet in terms of physical composition: bus ethernet and switched ethernet. Bus ethernet was used in the 1990s and had all of its nodes in the same domain, meaning collisions were likely. Switched ethernet is a setup with a switch in the middle of a bunch of nodes, each node running a separate ethernet instance to prevent collision. There are six parts of an ethernet frame:

 - Data field: This is where the IP datagram is. The maximum size is 1,500 bytes, and the minimum is 46 bytes. If a datagram exceeds 1,500 bytes, it is fragmented, and if it is shorter than 46 bytes, it is stuffed with arbitrary bits that are removed later.

 - Destination address: This holds the MAC address of the destination adapter.

 - Source address: This holds the MAC address that transmits the frame to the local area network.

 - Type field: This field holds the higher layer protocol being used, like Novell IPX or AppleTalk. It is used for demultiplexing once the data gets to the receiver.

 - Cyclic redundancy check: This is used to detect bit errors, as discussed before.

 - Preamble: This field synchronizes the clocks of the sender and the receiver. It is eight bytes long, with seven bytes of the sequence "10101010" and one byte of "10101011."

There is no handshaking in ethernet, so it is said to be connectionless. Therefore, it is also more unreliable than higher protocols like TCP; no ACKs or NAKs are sent in response to datagram receipts or the lack thereof. It is up to TCP to recover dropped data if that protocol is used. The MAC protocol ethernet uses is CSMA/CD with binary backoff. There are many different types of ethernet, including 10BASE-T, 10BASE-2, and 1000BASE-LX. The first part of these abbreviations stands for the speed of the type in Megabits per second (or Gigabits if it includes a "G"), "BASE" denotes that the network only carries ethernet traffic, and the suffix refers to the type of cable used. A new, faster kind of ethernet, Gigabit Ethernet, has point-to-point connection capabilities and is backwards compatible with 10BASE-T and 100BASE-T ethernet.
