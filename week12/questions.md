# Week 12 Questions

# R4: Suppose two nodes start to transmit at the same time a packet of length L over a broadcast channel of rate R. Denote the propagation delay between the two nodes as Dprop. Will there be a collision if Dprop < L/R? Why or Why not?

There will be a collision; while one node is transmitting, it is receiving a transmission from the other node.

# R5: In Section 6.3, we listed four desirable characteristics of a broadcast channel. Which of these characteristics does slotted ALOHA have? Which of these characteristics does token passing have?

Slotted ALOHA breaks transmissions into time slots, is simple and cheap, and has an average transmission rate of R/M bps. Token passing does all of the above and is fully decentralized.

# R6: In CSMA/CD, after the fifth collision, what is the probability that a node chooses K = 4? The result K = 4 corresponds to a delay of how many seconds on a 10 Mbps Ethernet?

The adapter will choose a value somewhere between 0 and 31, so the probability of choosing 4 is 1/32. This corresponds to 204.8 microseconds of delay.

# P1: Suppose the information content of a packet is the bit pattern 1110 0110 1001 0101 and an even parity scheme is being used. What would the value of the field containing the parity bits be for the case of a two-dimensional parity scheme? Your answer should be such that a minimum-length checksum field is used.

1100 0
0110 0
1001 0
0101 0

0110 0

# P3: Suppose the informatino portion of a packet (D in Figure 6.3) contains 10 bytes consisting of the 8-bit unsigned binary ASCII representation of string "Internet". Compute the Internet checksum for this data.

I 049

n 110

t 116

e 101

r 114

n 110

e 101

t 116

 0011000101101110

+0111010001100101


 1010010111000011

+0111001001101110


 0001100000110001

+0110010101110100
 

 0111110110100101 -> 1000001001011010 <- checksum

# P6: Consider the previous problem, but suppose that D has the value:
## a. 1000100101

0111011010

## b. 0101101010

1010010101

## c. 0110100011

1001011100

# P11: Suppose four activity nodes -- nodes A, B, C, D -- are competing for access to a channel using slotted ALOHA. Assume each node has an infinate number of packets to send. Each node attempts to transmit in each slot with probability p. The first slot is numbered slot 1, the second slot is numbered slot 2, and so on.
## a. What is the probability that node A succeeds for the first time in slot 4?

(1-p(A))^3(p(A))

## b. What is the probability that some node (either A, B, C, or D) succeeds in slot 5?

4p(1-p)^4

## c. What is the probability that the first success occurs in slot 4?

(1-(4p(1-p)^4))^3(4p(1-p)^4)

## d. What is the efficiency of this four-node system?

4p(1-p)^4

## P13: Consider a broadcast channel with N nodes and a transmission rate of R bps. Suppose the broadcast channel uses polling (with an additional polling node) for multiple access. Suppose the amound of time from when a node completes transmission until the subsequent node is permitted ot transmit (that is, the polling delay) is Dpoll. Suppose that within a polling round, a given node is allowed to transmit at most Q bits. What is the maximum thoughput of the broadcast channel?

Length of round: N(Q/R + Dpoll)

Max throughput: NQ/(N(Q/R + Dpoll))

Simplified: R/(1 + (RDpoll/Q))
