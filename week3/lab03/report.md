# Week 3 Lab Report: DNS with Wireshark

# Introduction

This was a lab meant for familiarization with DNS queries through examining what information Wireshark picks up on them. Given that I did this before any of the book exercises, I learned a ton. I found out that you have to clear your browser cache and system cache to see some DNS requests happen due to DNS caching, that my WSL IP is different than my IP for WiFi, and that nslookup conducts queries over all the IPs listed in my /etc/resolv.conf file.

# Process

I first ran nslookup on the host provided in the lab, www.iitb.ac.in. This was pretty straightforward; I just conducted a regular DNS query as well as an NS query for authoritative answers. After that, I flushed my system caches and cleared my Firefox browser cache to prep for packet capture with Wireshark. I had an interesting time trying to find the IP to capture over. I first tried the IP from a run of ifconfig in my WSL terminal, but I figured out that the IP it returned was different from the one that would return traffic. Once I found my system IP in Settings, I thought I had it set up, but I also eventually realized that I did not have "WiFi" selected to capture over, which is important. Once I had that worked out, everything went smoothly. I searched up the provided site, stopped the capture, and answered the questions below. The last part was using nslookup alongside Wireshark, and since I knew to use my WSL IP for that, I got some nice captures. The results of those captures are also noted below.

# Questions

# 1. Run nslookup to obtain the IP address of the web server for the Indian Institute of Technology in Bombay, India: www.iitb.ac.in. What is the IP address of www.iitb.ac.in?

The IP is 103.21.124.10.

# 2. What is the IP address of the DNS server that provided the answer to your nslookup command in question 1 above?

The address is 10.1.74.10.

# 3. Did the answer to your nslookup command in question 1 above come from an authoritative or non-authoritative server?

It came from a non-authoritative server.

# 4. Use the nslookup command to determine the name of the authoritative name server for the iit.ac.in domain. What is that name? (If there are more than one authoritative servers, what is the name of the first authoritative server returned by nslookup)? If you had to find the IP address of that authoritative name server, how would you do so?

The name of the server is dns1.iitb.ac.in, and you could run nslookup on that server. The address of it is 103.21.125.129.

# 5. Locate the first DNS query message resolving the name gaia.cs.umass.edu. What is the packet number in the trace for the DNS query message? Is this query message sent over UDP or TCP?

It is packet number 245, and it was sent via UDP.

# 6. Now locate the corresponding DNS response to the initial DNS query. What is the packet number in the trace for the DNS response message? Is this response message received via UDP or TCP?

It is packet number 246, and it was received via UDP.

# 7. What is the destination port for the DNS query message? What is the source port of the DNS response message?

They are both port 53.

# 8. To what IP address is the DNS query message sent?

It was sent to 192.168.1.1.

# 9. Examine the DNS query message. How many “questions” does this DNS message contain? How many “answers” answers does it contain?

It contains one question and no answers.

# 10. Examine the DNS response message to the initial query message. How many “questions” does this DNS message contain? How many “answers” answers does it contain?

It contains one question and no answers.

# 11. The web page for the base file http://gaia.cs.umass.edu/kurose_ross/ references the image object http://gaia.cs.umass.edu/kurose_ross/header_graphic_book_8E_2.jpg , which, like the base webpage, is on gaia.cs.umass.edu. What is the packet number in the trace for the initial HTTP GET request for the base file http://gaia.cs.umass.edu/kurose_ross/? What is the packet number in the trace of the DNS query made to resolve gaia.cs.umass.edu so that this initial HTTP request can be sent to the gaia.cs.umass.edu IP address? What is the packet number in the trace of the received DNS response? What is the packet number in the trace for the HTTP GET request for the image object http://gaia.cs.umass.edu/kurose_ross/header_graphic_book_8E2.jpg? What is the packet number in the DNS query made to resolve gaia.cs.umass.edu so that this second HTTP request can be sent to the gaia.cs.umass.edu IP address? Discuss how DNS caching affects the answer to this last question.

The packet numbers requested, in order, are as follows: 262, 245, 246, 406, and 245. The packet with the DNS query allowing the second HTTP request to be sent is the original DNS query packet because DNS caching is enabled. DNS caching allows a system to make a unique DNS request only once and fast-track queries to that host in the future.

# 12. What is the destination port for the DNS query message? What is the source port of the DNS response message?

They are both port 53.

# 13. To what IP address is the DNS query message sent? Is this the IP address of your default local DNS server?

There were queries sent to 10.1.74.10, 8.8.8.8, and 172.21.192.11. Those are the DNS IPs that I have configured for WSL.

# 14. Examine the DNS query message. What “Type” of DNS query is it? Does the query message contain any “answers”?

It was a Type A query, and there were no answers.

# 15. Examine the DNS response message to the query message. How many “questions” does this DNS response message contain? How many “answers”?

There was one question and one answer.

# 16. To what IP address is the DNS query message sent? Is this the IP address of your default local DNS server?

The message was sent to the same three local DNS IPs as before.

# 17. Examine the DNS query message. How many questions does the query have? Does the query message contain any “answers”?

It had one question and no answers.

# 18. Examine the DNS response message. How many answers does the response have? What information is contained in the answers? How many additional resource records are returned? What additional information is included in these additional resource records?

There were three answers, each of which contained a non-authoritative DNS server, the response type, and the response class (more information in the third pcap). No additional resource records were returned. 
