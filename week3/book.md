# Week 3 Questions

# R16. Suppose Alice, with a Web-based e-mail account (such as Hotmail or Gmail), sends a message to Bob, who accesses his mail from his mail server using IMAP. Discuss how the message gets from Alice's host to Bob's host. Be sure to list the series of application-layer protocols that are used to move the message between the two hosts.

Since Alice is using a Web-based e-mail, her user agent will use HTTP to deliver her mail to her mail server. The mail server will then use SMTP to send the e-mail to Bob. Once Alice's message is in her message queue, a TCP connection will be opened to Bob's mail server. Once the three-way handshake is completed, the e-mail is sent through the TCP connection to Bob's mail server. Since Bob uses IMAP to get his mail, IMAP is the protocol that will retrieve the e-mail from Bob's server.

# R18. What is the HOL blocking issue in HTTP/1.1? How does HTTP/2 attempt to solve it?

From last week's learning objectives:

"HTTP/2 was created to alleviate the Head of Line (HOL) blocking problem caused by sending Web page data over a single TCP connection. Because of the singular connection that sites using HTTP 1.1 had, smaller objects on a page would have to wait to load until larger ones loaded. HTTP/2 works around this not by adding more TCP connections (which sites using 1.1 still have to do), but by breaking messages into frames that are sent at different times around the incoming HTTP responses. With this type of framing implemented, smaller objects are loaded before larger ones because they take fewer frames to fully render."

# R24. CDNs typically adopt one of two different server placement philosophies. Name and briefly describe them.

From last week's learning objectives:

"There are two main ways in which CDNs set up their servers: using the "Enter Deep" method or the "Bring Home" method. The "Enter Deep" method consists of placing CDN clusters within ISPs that allow them to get as close as possible to users, therefore eliminating as much delay as possible and ensuring throughput. The "Bring Home" method consists of placing CDN clusters within IXPs rather than dispersing them between ISPs. This means less maintenance for providers but slower service for users than with the "Enter Deep" method."

# P16. How does SMTP mark the end of a message body? How about HTTP? Can HTTP use the same method as SMTP to mark the end of a message body? Explain.

SMTP ends a message using one line with only a period on it while HTTP indicates the length of the message in the "Content-Length:" response header to limit it. These two protocols cannot use the same method of ending messages because SMTP must be in 7-bit ASCII whereas the content type of an HTTP response can vary.

# P18a./b. What is a "whois" database? Use various whois databases on the Internet to obtain the names of two DNS servers. Indicate which whois databases you used.

A "whois" database is a database of domains that exist on the Internet. You can use it to find out if a domain you want to register is already taken or who owns a particular domain name. I searched up "google.com" on GoDaddy's whois database, and it returned 4 DNS servers: ns1.google.com, ns2.google.com, ns3.google.com, and ns4.google.com. I also searched up "godaddy.com" in its own whois database, and it returned the following servers:

Name Server: CNS1.GODADDY.COM
Name Server: CNS2.GODADDY.COM
Name Server: A11-64.AKAM.NET
Name Server: A1-245.AKAM.NET
Name Server: A20-65.AKAM.NET
Name Server: A6-66.AKAM.NET
Name Server: A8-67.AKAM.NET
Name Server: A9-67.AKAM.NET

# P20. Suppose you can access the caches in the local DNS servers of your department. Can you propose a way to roughly determine the Web servers (outside your department) that are most popular among the users in your department? Explain.

You could grep the cache for unique strings and count the number of times each line appears in the cache. The line with the highest count would be the most popular Web server used within the department. This is possible because every time a host from within the local server requests a certain Web server, that request is mapped in the cache. It also speeds up the query process because a separate DNS query doesn't have to be made for Web servers that have already been requested in the past.

# P21. Suppose that your department has a local DNS server for all computers in the department. You are an ordinary user (i.e. not a network/system administrator). Can you determine if an external Web site was likely accessed from a computer in your department a couple of seconds ago? Explain.

If the requests are not being cached, I could listen on the IP address of the local DNS server in Wireshark and see any DNS queries that are made over that server. If the queries are being cached, I would have to determine when/how that cache is cleared in order to have a 100% chance of finding a DNS query in my packet capture.
