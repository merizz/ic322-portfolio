# Week 3 Learning Objectives

# 1. I can explain how the DNS system uses local, authoritative, TLD, and root servers to translate an IP address into a hostname.

DNS uses a three-tier hierarchy of servers to resolve a hostname. On the bottom are root servers. There are hundreds of root servers that are copies of 13 unique root servers overseen by 12 organizations, all of which is coordinated through the Internet Assigned Numbers Authority (IANA). These servers provide the IPs for Top-Level Domain (TLD) servers, which are next up in the hierarchy. TLDs are found at the ends of Web addresses (com, net, edu, fr, uk, etc.) and are managed by their own servers. TLD servers give IPs to the last tier in the hierarchy, which holds the authoritative DNS servers. Organizations with hosts available to the public have these kinds of servers that map those hosts to IPs. Organizations can manage their own authoritative DNS servers or pay to have them managed by a service provider. 
Local DNS servers are not part of the hierarchy but still play a role in DNS queries. They are part of ISPs, and they act as  a go-between for hosts and the DNS hierarchy. ISPs give a connected host IPs for their DNS servers, and those servers forward queries to the hierarchy.

To put it in example format, if a host is requesting the IP of another host, the local DNS server sends the query to a root server, the root server gives a list of IPs for sites with the same TLD as the requested site back to the local DNS server, the local server sends the list to the TLD server, that server sends the IP for the authoritative server of the domain back, and the local server finally sends a query for the IP of the requested host to the authoritative server.

# 2. I can explain the role of each DNS record type.

DNS records give the user mappings between hostnames and IPs. There are four types of DNS record: "A", "NS", "CNAME", and "MX". Type A records give the standard map between hostnames and IPs, Type NS records route a query to an authoritative DNS server that has a requested IP, Type CNAME records give hosts canonical hostnames they need, and Type MX records provide mail servers with simple aliases for their hostnames.

# 3. I can explain the role of the SMTP, IMAP, and POP protocols in the email system.

SMTP transfers e-mails between hosts. Sent mail must be in 7-bit ASCII format, and there typically are not any protocols between the connected hosts besides SMTP. Before mail is sent, there are TCP and SMTP handshaking phases to establish a connection. The e-mail addresses of both the sender and the recipient are indicated, and the mail is sent via TCP for security reasons. 

IMAP (Internet Mail Access Protocol) is a way of retrieving mail from the SMTP server once it has been sent. It helps the user receiving the mail interface with the correspondence and manage an inbox. An example of this is Microsoft Outlook. 

According to the Microsoft Support site, POP (Post Office Protocol) is similar to IMAP in that it is a mail access protocol, but it is more primitive. It only allows users to download e-mails from the SMTP server, and it only lets this happen from one device. The IMAP interface has much more capability, allowing users to customize their mailbox and access their mail from multiple devices.  

# 4. I know what each of the following tools are used for: nslookup, dig, whois.

The tool "nslookup" is used to make queries directly from the terminal you are working in to a specific host. The tool "dig" expands on this functionality by offering information on the finer details of the query rather than just the resulting domain names, like an outline of the questions and answers. A "whois" database is a way of finding out whether or not a domain name is taken on the Internet as well as supporting information about that domain name, such as the server that runs it. 
