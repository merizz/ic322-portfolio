# Week 13 Learning Objectives

# 1. I can describe the role a switch plays in a computer network.

A switch is a device that is central to the functionality of the link layer. Its purpose is to store and forward Ethernet frames. It performs this task by looking at the MAC address of the Ethernet frame, forwarding it to the correct links. It has matches between MAC addresses and interfaces stored in a switch table. It uses CSMA/CD protocol to avoid collisions of transmissions going to the same device, but collisions are uncommon because every transmission has its own link. The device is transparent from the perspective of hosts, meaning it cannot be identified by a host. Switches also learn how to operate on their own and do not need to be configured, much like LLMs; they simply update their switch tables with new information when needed.

When a frame arrives at a switch, the link and the MAC address are recorded in the switch table. The destination link for the address is searched for in the table, and if it is found, the frame is forwarded over that link (if the link is the one the frame came from, it is dropped). If no destination link is found, the switch "floods," or sends the frame over all links besides the origin link.

# 2. I can explain the problem ARP solves, how it solves the problem, and can simulate an ARP table given a simple LAN scenario.

ARP, or address resolution protocol, solves the problem of finding an interface's MAC address given its IP address. Each host and router in a local area network has an ARP table that maps each interface MAC address to an IP address. It also includes the time-to-live (TTL) of the mapping; since IP addresses within a network can be temporary, the mappings between those addresses and their devices are also temporary. An example of how ARP is used is included in this directory.

# 3. I can explain CSMA/CA, how it differs from CSMA/CD, what problems it addresses, and how it solves them.

CSMA/CA is a carrier sense multiple access protocol just like CSMA/CD, meaning it transmits a frame when it senses that the link it will be sent over is not busy. These types of protocols were created to mitigate collisions between transmitted frames that cause frames to be dropped and transmission rates to be slower. There are two main differences between CSMA/CA and CSMA/CD. The first is that CSMA/CA implements collision avoidance (hence the CA) rather than collision detection. This is done for two reasons: 802.11 signal is not strong enough to send and receive data simultaneously, and signal fading and physical obstructions cause indetectable collisions on their own. CSMA/CA also implements a link-layer acknowledgment scheme (ARQ) because 802.11 transmission are more error-prone than Ethernet transmissions.

Since CSMA/CA cannot detect collisions, the entire frame is sent once the channel is clear; fragmentation increases the risk of collision. ARQ is used to determine whether or not a frame got to its destination intact. The destination waits a certain period of time before sending back an acknowledgment; this process is known as Short Inter-frame Spacing (SIFS). If no ACK is received, the frame is retransmitted, and if this problem occurs a certain number of times in a row, the frame is discarded completely.

To determine when to send a frame, CSMA/CA detects whether or not the channel is empty and sends the frame after a period known as Distributed Inter-frame Space (DIFS). If the channel is suddenly busy, the protocol uses binary exponential backoff to wait for a random time to transmit after the DIFS period. These waiting periods are what set CSMA/CA apart from CSMA/CD in terms of process. 
