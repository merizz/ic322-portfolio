# Week 13 Questions

# R10. Suppose nodes A, B, and C each attach to the same broadcast LAN (through their adapters). If A sends thousands of IP datagrams to B with each encapsulating frame addressed to the MAC address of B, will C's adapter process these frames? If so, will C's adapter pass the IP datagrams in these frames to the network layer C? How would your answers change if A sends frames with the MAC boradcast address?

C's adapter will process the frames, but it will not pass the frames up to the network layer; this will only happen if the MAC broadcast address is known.

# R11. Why is an ARP query sent within a broadcast frame? Why is an ARP response sent within a frame with a specific destination MAC address?

The ARP query is sent to figure out the target MAC address of the data. Each node that receives the broadcast checks the IP against its ARP table to see if it has the desired MAC address. If it does, that node sends the MAC address in an ARP response to the sender.

# P14. Consider three LANs interconnected by two routers, as shown in Figure 6.33.

## a. Assign IP addresses to all of the interfaces. For Subnet 1 use addresses of the form 192.168.1.xxx; for Subnet 2 use addresses of the form 192.168.2.xxx; and for Subnet 3 use addresses of the form 192.168.3.xxx.

## b. Assign MAC addresses to all of the adapters.

## c. Consider sending an IP datagram from Host E to Host B. Suppose all of the ARP tables are up to date. Enumerate all the steps, as done for the single-router example in Section 6.4.1.

## d. Repeat (c), now assuming that the ARP table in the sending host is empty (and the other tables are up to date).

This solution is in a separate file.

# P15. Consider Figure 6.33. Now we replace the router between subnets 1 and 2 with a switch S1, and label the router between subnets 2 and 3 as R1.

## a. Consider sending an IP datagram from Host E to Host F. Will Host E ask router R1 to help forward the datagram? Why? In the Ethernet frame containing the IP datagram, what are the source and destination IP and MAC addresses?

E will not ask R1 to help forward the datagram because E can see that F is in the same network based on the subnet prefix of F's IP. The source IP and MAC are E's IP and MAC, and the destination IP and MAC are F's IP and MAC.

## b. Suppose E would like to send an IP datagram to B, and assume that E's ARP cache does not contain B's MAC address. Will E perform an ARP query to find B's MAC address? Why? In the Ethernet frame (containing the IP datagram destined to B) that is delivered to router R1, what are the source and destination and MAC addresses?

E cannot perform an ARP query for B's MAC because they are not in the same network. E must find out what B's MAC is based on B's IP address. The source IP and MAC are E's IP and MAC, and the destination IP and MAC are B's IP and the MAC of the interface connecting R1 to Subnet 3, respectively.

## c. Suppose Host A would like to send an IP datagram to Host B, and neither A's ARP cache contains B's MAC address nor does B's ARP cache contain A's MAC address. Further suppose that the switch S1's forwarding table contains entries for Host B and router R1 only. Thus, A will broadcast an ARP request message. What actions will switch S1 perform once it receives the ARP request message? Will router R1 also receive this ARP request message? If so, will R1 forward the message to Subnet 3? Once Host B receives this ARP request message, it will send back to Host A an ARP response message. But will it send an ARP query message to ask for A's MAC address? Why? What will switch S1 do once it receives an ARP response message from Host B?

S1 will figure out Host A's MAC address, create an entry for Host A in its ARP table, and send an ARP response back to Host A. R1 will receive the same ARP broadcast message, but it will not forward the message to a different subnet. Since A's MAC address is in the ARP broadcast it sent originally, B will not send another ARP query to figure out what A's MAC is. Since A and B are on the same LAN, S1 will add B to its forwarding table and drop the transmission.

# R3. What are the differences between the following types of wireless channel impairments: path loss, multipath propagation, interference from other sources?

Path loss is the overall loss of signal strength, multipath propagation is loss through using multiple paths per signal, and interference from other sources occurs when signals of the same frequency as the transmission cause loss.

# R4. As a mobile node gets farther and farther away from a base station, what are two actions that a base station could take to ensure that the loss probability of a transmitted frame does not increase?

A base station could either increase the power of the transmission or decrease the rate of transmission to be more deliberate about getting a single transmission through.

# P6. In step 4 of the CSMA/CA protocol, a station that successfully transmits a frame begins the CSMA/CA protocol for a second frame at step 2, rather than at step 1. What rationale might the designers of CSMA/CA have had in mind by having such a station not transmit the second frame immediately (if the channel is sensed idle)?

The creators of the protocol wanted to make sure every node has a fair chance of transmitting in a timely manner. If a node were to transmit at step 1, it would not go through binary backoff, meaning a second node would likely be waiting much longer for the first node to transmit before it could use the channel.

# P7. Suppose an 802.11b station is configured to always reserve the channel with the RTS/CTS sequence. Suppose this station suddenly wants to transmit 1,500 bytes of data, and all other stations are idle at this time. As a function of SIFS and DIFS, and ignoring propagation delay and assuming no bit errors, calculate the time required to transmit the frame and receive the acknowledgment.

Frame w/o data -> 32 bytes -> 256 bits

Time to transmit control frame -> 256 bits/11 Mbps = 23 microseconds

Time to transmit data frame -> 12256 bits/11 Mbps = 1114 microseconds

Time for frame and ACK = DIFS + 3*SIFS + (3*23 + 1114) -> DIFS + 3*SIFS + 1183 microseconds
