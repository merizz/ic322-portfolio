# Peer Feedback and Lab

m242628/ic322-portfolio#22

For the lab, see Nick Heil's writeup from last week. The addition we made to improve the throughput was the compression algorithm in the original solution to the problem. The combination of the semafore and the compression topped the original throughput we achieved.
