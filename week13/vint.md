# Vint Cerf Questions

# 1. Vint Cerf is known as "one of the fathers of the Internet". Why is he known as this?

Vint Cerf is known as a Father of the Internet because he helped create TCP/IP protocols, largely contributing to the structure of the Internet.

# 2. Find 3 surprising Vint Cerf facts.

 - He was the vice-president of Google in 2005.

 - He worked on one of the original ARPANET nodes (first packet-switched network).

 - He was a member of government panels on cybersecurity and the national information infrastructure.

# 3. Develop 2 interesting and insightful questions you'd like to ask him. For each question, describe why it is interesting and insightful.

 - What was it like watching the Internet blossom into what it is today? (he would have witnessed most of the Internet's creation and exponential expansion)

 - What is your favorite work of science-fiction? (He is apparently a big fan of the genre)

# source: https://www.britannica.com/biography/Vinton-Cerf
