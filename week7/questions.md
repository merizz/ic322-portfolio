# Week 7 Questions

# R11: Describe how packet loss can occur at input ports. Describe how packet loss at input ports can be eliminated (without using infinite buffers).

If the router's packet queue is too large due to a slow switch fabric speed, the router will not have enough buffer space, and packets will inevitably be lost. To eliminate this issue without using infinite buffers, the switch fabric speed must be as many times as fast as the input line speed as the number of input ports.

# R12: Describe how packet loss can occur at output ports. Can this loss be prevented by increasing the switch fabric speed?

This is caused by the same issue causing packet loss at input ports, where the packet queue grows so large that the entire router buffer is full. The packets are just lost at the end of the router rather than the beginning. It can also be fixed by increasing the switch fabric speed.

# R13: What is HOL Blocking? Does it occur in input ports or output ports?

"HOL" stands for "Head of Line," and this issue is just what it sounds like: A packet must wait in a queue to move through the router because there is another packet at the head of the line. It occurs at input ports.

# R16: What is an essential difference between RR and WFQ packet scheduling? Is there a case (hint: consider the WFQ weights) where RR and WFQ will behave exactly the same?

In RR (Round Robin) scheduling, packets are divided into classes, and the scheduler visits each class to ensure packet starvation does not happen. All of the packets get equal priority. In WFQ (Weighted Fair Queuing) scheduling, the same process as Round Robin occurs, but each class is assigned a priority (weight). If that WFQ scheduler were to assign equal weights to each class of packets, it would behave exactly like RR scheduling.

# R18: What field in the IP header can be used to ensure that a packet is forwarded through no more than N routers?

The Time-to-Live field makes sure the above does not happen based on how long the packet has before it must reach its destination.

# R21: Do routers have IP addresses? If so, how many?

Routers have a single IP address for each router interface.

# P5: Suppose that the WFQ scheduling policy is applied to a buffer that supports three classes, and suppose the weights are 0.5, 0.25, and 0.25 for the three classes.
## a. Suppose that each class has a large number of packets in the buffer. In what sequence might the three classes be served in order to achieve the WFQ weights? (For round robin scheduling, a natural sequence is 123123123...).

One sequence that could work is 1213121312... The first class will be visited every other cycle because its priority is highest. The second and third classes will be visited between those cycles, starting with the second class because the last two classes have the same weight and the second class is listed first.

## b. Suppose that classes 1 and 2 have a large number of packets in the buffer, and there are no class 3 packets in the buffer. In what sequence might the three classes be served in to achieve the WFQ weights?

One possible configuration is 112112112... This ensures the first class is visited twice as much as the second class, and the third class does not have to be considered here.

# P8: Consider a datagram network using 32-bit host addresses. Suppose a router has four links, numbered 0 through 3, and packets are to be forwarded to the link interfaces as follows: (image in textbook)
## a. Provide a forwarding table that has 5 entries, uses longest prefix matching, and forwards packets to the correct link interfaces.

My answer is attached as a picture.

## b. Describe how your forwarding table determines the appropriate link interface for datagrams with destination addresses: (image in textbook)

The first address listed matches the "otherwise" category, so it goes to link 3. The second address starts with "11100001 01," so it goes to link 2. The third address starts with "11100001 10," so it goes to link 3 as well.

# P9: Consider a datagram network using 8-bit host addresses. Suppose a router uses longest prefix matching and has the following forwarding table: (image in textbook). For each of the four interfaces, give the associated range of destination host addresses and the number of addresses in the range.

Interface 0:

 - Range: 00000000-00111111, 2^6 or 64 addresses available

Interface 1:

 - Range: 01000000-01111111, 2^5 or 32 addresses available

Interface 2:

 - Range: 01100000-01111111 and 10000000-10111111, 2^6+2^5 or 96 addresses available

Interface 3:

 - Range: 11000000-11111111, 2^6 or 64 addresses available

# P11. Consider a router that interconnects three subnets: Subnet 1, Subnet 2, and Subnet 3. Suppose all of the interfaces in each of these three subnets are required to have the prefix 223.1.17/24. Also suppose that Subnet 1 is required to support at least 60 interfaces, Subnet 2 is to support at least 90 interfaces, and Subnet 3 is to support at least 12 interfaces. Provide three network addresses (of the form a.b.c.d/x) that satisfy these constraints.

Subnet 1: 60 interfaces -> round up to 64 -> need 6 bits for host I.D. (2^6 = 64)
Subnet 2: 90 interfaces -> round up to 128 -> need 7 bits for host I.D. (2^7 = 128)
Subnet 3: 12 interfaces -> round up to 16 -> need 4 bits for host I.D. (2^4 = 16)

There are 32 bits in an IP address.

32-6 = 26 bits for subnet 
32-7 = 25 bits for subnet
32-4 = 28 bits for subnet

Three possible addresses satisfying the above constraints are as follows:

Subnet 1: 223.1.17.128/26
Subnet 2: 223.1.17.0/25
Subnet 3: 223.1.17.192/28

# P4. Consider the switch shown below (in the book). Suppose that all datagrams have the same fixed length, that the switch operates in a slotted, synchronous manner, and that in one time slot a datagram can be transferred from an input port to an output port. The switch fabric is a crossbar so that at most one datagram can be transferred to a given output port in a time slot, but different output ports can receive datagrams from different input ports in a single time slot. What is the minimal number of time slots needed to transfer the packets shown from input ports to their output ports, assuming any input queue scheduling order you want (i.e., it need not have HOL blocking)? What is the largest number of slots needed, assuming the worst-case scheduling order you can devise, assuming that a non-empty input queue is never idle?

Since there are three input ports in the diagram, three time slots are needed at minimum.

Since there are five packets across each of the input ports, the worse-case scheduling order is 5. This is assuming each of those packets is given its own time slot.
