# Week 7 Learning Objectives

# 1. I can explain what a subnet is, how a subnet mask is used, and how longest prefix matching is used to route datagrams to their intended subnets.

According to the slides for this week, a subnet is made of "device interfaces that can physically reach each other without passing through an intervening router." In other words, if you compile all of the connections going into or coming out of a router, those sets of connections make up subnets. Given an IP address defined by the format "a.b.c.d/x" (CIDR), the subnet mask indicated by "/x" tells a router that the leftmost 'x' bits of the 32-bit address are the subnet address for the host behind the IP address. A router outside that subnet only needs to look at those leftmost bits because it only needs to send packets to the subnet of the IP, making forwarding tables (discussed below) much more simple than containing whole address matches.

A forwarding table is a table of address prefixes matched with output link interfaces telling the router which link to send a packet from a specific IP through. The router looks at the subnet of an incoming IP and finds the longest matching prefix of an address in the table, sending the packet through the associated output link. This process is called "longest prefix matching" and, along with the use of subnet masks, contributes to the simplicity of routing packets.

# 2. I can step though the DHCP protocol and show how it is used to assign IP addresses.

Dynamic Host Configuration Protocol, or DHCP, has the goal of assigning users IP addresses dynamically. This means that any time a user joins a network using this protocol, they are given a new IP address. Each address is assigned a lease period, and that lease can be renewed if the user stays on the network long enough. DHCP also contributes to the conservation of the relatively-small bank of existing IPs by reassigning a user's IP each time they establish a connection. This protocol is an efficient way of integrating mobile users into networks because of their constant change in location and connection status.

A DHCP server is usually located in the same place as a router, serving all of the subnets the router is connected to. When the server assigns an incoming host an IP address, it will give the host an address that is part of a subnet it serves. There are four steps to assigning a host an IP address:

 1. DHCP Server Discovery: The client first sends a DHCP discover message in a UDP packet to port 67. The destination IP of this message is 255.255.255.255, and it is labeled with a source IP of 0.0.0.0, as the host sending the message has not yet been assigned an IP on the network. 

 2. DHCP Server Offers: If a DHCP server receives a discover message, it sends a DHCP offer message back over the same destination IP the host used. It gives the host a transaction ID, proposed IP address, network mask, and proposed lease time.

 3. DHCP Request: If a host accepts the server offer, it sends back a DHCP request that echoes the parameters from the offer.

 4. DHCP ACK: The server confirms the transaction with an acknowledgment message.

Numerically speaking, each subnet is allotted a certain amount of interfaces for IPs, and each IP is 32 bits. Specific address ranges are created by figuring out how much of each address is for the subnet mask and how much is for the host ID. 
