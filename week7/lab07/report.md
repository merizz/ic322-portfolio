# Week 7 Lab Report

# Introduction

This lab was meant to teach the basics of networking through Python and was also a way for us to Beta-test the Protocol Pioneer game. One thing I learned (was refreshed on, rather) was how to cast variables in Python.

# Process

For the first chapter, I was initially confused on what I needed to do to progress through the game, but I eventually realized I needed to edit the message that was being sent from my ship to the one the Mothership was expecting and change the interface to "W" instead of "N" to match the location of the Mothership. Figuring out how to run my solution was also a bit unclear to me, but I found that restarting the kernel and running all cells was the way to accomplish this.

The second problem went a bit more smoothly for me. I added a few lines to the loop receiving messages across each of the interfaces the repair ships were using to both add the numbers for each ship and increment the number of messages received from each. I added a conditional statement to check if three messages had been received from a given ship and sent the current sum back if so. I then set the message count and sum for that ship back to zero.

One hiccup I ran into was finding out I had to cast the message to an int to add the number and cast it back to a string to send the sum, but I figured that out soon enough.

Both of my solutions are included as screenshots.

# Questions

My solutions are detailed in the above section.

## Testing Notes

Overall, I thought the game was pretty fun! One thing I would fix is the clarity of the instructions for each chapter. Part of the challenge was figuring out what to do in each scenario, but especially for the first chapter, I thought the end goal should have been made a bit more clear. I would also suggest adding in a note about how to run the solution you come up with; it took me a bit of poking around to figure that out. I did not find any bugs from the testing I ran.
