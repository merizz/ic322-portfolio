# Week 9 Lab Report

# Introduction

This lab was meant to teach the basics of networking through Python and was also a way for us to Beta-test the Protocol Pioneer game. I learned a lot about the JSON library and using dictionaries to format datagrams. I also saw how the distance vector algorithm is implemented in Python for faster transmission.

# Process

For this chapter, I mostly followed the process of the instructor solution with a few tweaks to allow me to get in the weeds with JSON. I got rid of most of the functions, save for the distance-vector method, and hard coded all of the dictionaries. Most of my issues in debugging dealt with putting a string where a dictionary should have been or a dictionary where a string should have been for parsing and/or performing the distance vector algorithm.

The overall process for the drones was either sending a message if the shortest path to the receiver was found or putting the datagram through the distance vector algorithm. If the path to a destination node could not be determined from what a drone's neighbor drones were aware of, the datagram was given an arbitrary source, destination, and interface. The player went through the same process as the drones but set a key and its expiration in the datagrams they sent to Sawblade. Sawblade parsed the datagrams he received from the player for the key and expiration, and if the key had not expired (which was determined by how many ticks there were), Sawblade would operate the radio with the key. He performed the distance vector algorithm in the same way the other two entities did.

The solution is included in this directory.

# Testing Notes

This level was way more challenging than the others, especially before the instructor solution was released. My suggestion would be to talk more about Python libraries that could help the player solve the level or possibly hint at what aspects of class would be helpful.
