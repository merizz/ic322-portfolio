# Week 9 Questions

# R5. What is the “count to infinity” problem in distance vector routing?

This problem occurs when a link weight within a loop of routers is increased to a very large number, meaning the datagram will have to wait a very long time for the least-cost path to be recalculated when the right path is the link opposite the extremely heavy one.

# R8. True or false: When an OSPF route sends its link state information, it is sent only to those nodes directly attached neighbors. Explain. 

This is false; OSPF is a protocol that uses the link state algorithm, which means its link state information is sent to all nodes rather than its neighbors as in the distance vector algorithm.

# R9. What is meant by an area in an OSPF autonomous system? Why was the concept of an area introduced?

Areas are smaller groups of systems. They were made to help link state algorithm calculations go faster by reducing the scope of the advertisements of updated paths from nodes as well as lower the number of routers and paths each router has to keep track of.

# P3. Consider the following network (in the book). With the indicated link costs, use Dijkstra’s shortest-path algorithm to compute the shortest path from x to all network nodes. Show how the algorithm works by computing a table similar to Table 5.1. 

My answer is included as an image in this directory.

# P4. Consider the network shown in Problem P3. Using Dijkstra’s algorithm, and showing your work using a table similar to Table 5.1, do the following:

My answers to the following parts are included as images in this directory.

## c. Compute the shortest path from v to all network nodes.



## d. Compute the shortest path from w to all network nodes.



## e. Compute the shortest path from y to all network nodes.



# P5. Consider the network shown below (in the book), and assume that each node initially knows the costs to each of its neighbors. Consider the distance-vector algorithm and show the distance table entries at node z.

My answer is included as an image in this directory.

# P11. Consider Figure 5.7. Suppose there is another router w, connected to router y and z. The costs of all links are given as follows: c(x,y) = 4, c(x,z) = 50, c(y,w) = 1, c(z,w) = 1, c(y,z) = 3. Suppose that poisoned reverse is used in the distance-vector routing algorithm.

## a. When the distance vector routing is stabilized, router w, y, and z inform their distances to x to each other. What distance values do they tell each other?

Router z tells router w that the distance from z to x is infinite (the poisoned reverse) and router y that the distance from z to x is 6. Router w tells router y that the distance from w to x is infinite and router z that the distance from w to x is 5. Router y tells router w that the distance from y to x is 4 and router z that the distance from y to x is 4.

## b. Now suppose that the link cost between x and y increases to 60. Will there be a count-to-infinity problem even if poisoned reverse is used? Why or why not? If there is a count-to-infinity problem, then how many iterations are needed for the distance-vector routing to reach a stable state again? Justify your answer. 

There will be a count-to-infinity problem because the link cost between x and y is drastically increased to 60 and there are two loops instead of one; poisoned reverse only fixes the problem for one loop. The loop will stabilize by the 31st iteration, which makes sense because the iteration starts at a weight of 9 and goes up by three each time (the weight between y and z).

## c. How do you modify c(y,z) such that there is no count-to-infinity problem at all if c(y,x) changes from 4 to 60?

The link between y and z should be cut, getting rid of the second loop.
