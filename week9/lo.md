# Week 9 Learning Objectives

# 1. I can describe how Link State algorithms work, their advantages, their common problems and how the problems are addressed. I can also give an example of a protocol based on the Link State algorithm.

Link state algorithms are classified as global routing algorithms, meaning that all of the routers on the path have access to the whole topology of the network and the cost of each link. One such algorithm is Dijkstra's algorithm. It is a centralized algorithm, meaning network topology and link costs are known to all nodes. It calculates the quickest path between one node and all the other nodes in the network, creating a forwarding table for that specific node. It is iterative in the way that it does this, going through many cycles to find the paths that cost the least. Here is the notation associated with Dijkstra's algorithm:

 - c(x,y) is the direct link cost from x to y; this cost is infinite if they are not directly connected

 - D(v) is a running estimate of the lowest cost from a source to destination Vector

 - p(v) is a "predecessor" node on the way from the source node to Vector

 - N' is the set of nodes for which the least-cost paths have been found.

A file with a worked-out example of the algorithm can be found in this directory.

One drawback of link state algorithms is oscillation. This is where datagrams from a bunch of hosts are headed to the same host and detect better network routes to that host simultaneously. The data makes a back-and-forth movement through the network as redirections occur, hence the name of the issue. The solution to this problem is each router sending out data at a random time to avoid synchronization of transmissions.

A couple of advantages to link state algorithms are speed of convergence and robustness. Link state algorithms typically go through fewer iterations than distance vector algorithms (discussed below), making them faster. Since each node makes its own forwarding table and route calculations are separated in link state algorithms, they are considered robust. 

A protocol that uses the link state algorithm is OSPF.

# 2. I can describe how Distance Vector algorithms work, their advantages, their common problems and how the problems are addressed. I can also give an example of a protocol based on the Distance Vector algorithm.

The distance vector algorithm is a way of calculating the least-cost paths for datagrams for each node router in a subnet. Each node is sent the shortest paths to other nodes from its neighbors, and if one or more of the paths is shorter than a path the node has catalogued, the node updates that path, sending any results to its neighbors. This cycle continues until all nodes have the shortest possible path to all other nodes. 

The Bellman-Ford equation is used to calculate the distance between two nodes and find the minimum distance from a source node to a destination node. It consists of adding the link cost between the source node and its direct neighbor to the least-cost path between the neighbor and the destination node for all of the source node's neighbors. The minimum of these values is found and becomes the source node's new least-cost path to the destination node if it needs to be updated.

A file with a worked-out example of the algorithm can be found in this directory.

One advantage of the distance vector algorithm is that it's self-stopping, meaning that it only sends updates for paths if they need to be updated. This algorithm may go through more iterations to find the least-cost paths for nodes, but it wastes less information than link state algorithms in ony sending essential updates.

A disadvantage of the distance vector algorithm is the count-to-infinity problem. This issue happens when the link weight on one side of a loop between nodes is increased to a very large number, causing a datagram to get stuck waiting for the least-cost path to recalculate over a large number of iterations. This issue can be fixed for a single loop by implementing poisoned return, which is labeling the link cost to the increased link as infinity, making the destination "unreachable" via that link.

A protocol that uses the distance vector algorithm is RIP.
