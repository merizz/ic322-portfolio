# Week 4 Questions

# R5. Why is it that voice and video traffic is often sent over TCP rather than UDP  in today’s Internet? (Hint: The answer we are looking for has nothing to do  with TCP’s congestion-control mechanism.)

Voice and video correspondence rely on the packets being sent in the correct order as well as none of the packets being missing. Therefore, TCP is the best protocol to use; it ensures secure and reliable transfer.

# R7 Suppose a process in Host C has a UDP socket with port number 6789. Suppose both Host A and Host B each send a UDP segment to Host C with destination port number 6789. Will both of these segments be directed to the same socket at Host C? If so, how will the process at Host C know that these two segments originated from two different hosts?

Since a UDP socket consists only of a destination IP address and a destination port number, two segments heading to the same destination will use the same socket. The process at Host C will be able to distinguish them because they either differ in port number, IP address, or both.

# R8. Suppose that a Web server runs in Host C on port 80. Suppose this Web  server uses persistent connections, and is currently receiving requests from  two different Hosts, A and B. Are all of the requests being sent through the  same socket at Host C? If they are being passed through different sockets, do  both of the sockets have port 80? Discuss and explain.

Web servers function using TCP protocol, meaning each request goes to a separate socket. However, because this is a Web server running on port 80, both of the sockets have port 80. Similarly to how UDP handles multiple requests at a time, Web servers can tell hosts apart based on different IPs and/or source port numbers.

# R9. In our rdt protocols, why did we need to introduce sequence numbers?

Sequence numbers are a solution to the problem of corrupted ACKs or NAKs. Without them, if a packet was corrupted, the receiving host would not be able to tell whether or not the packet they were receiving was a retransmission to rectify a corrupted packet or a transmission of the next packet in the sequence. Sequence numbers use mod-2 arithmetic labeling to indicate if a packet is forward in the sequence or a repeated transmission.

# R10. In our rdt protocols, why did we need to introduce timers?

Timers exist to deal with packet loss. When a packet is lost, there is no way of knowing exactly what happened from the receiver end, so a retransmission must occur from the sender. In order to ensure the packet was really lost, the sender implements a timer that starts when the packet is sent, and if the packet is not received during an estimated period of delay, it is retransmitted.

# R11. Suppose that the roundtrip delay between sender and receiver is constant and  known to the sender. Would a timer still be necessary in protocol rdt 3.0,  assuming that packets can be lost? Explain.

A timer would likely still be necessary even with a constant roundtrip delay; processing time for the packet at the receiving end may not be constant, and the receiver still needs some way of knowing when to interrupt the sender in the case of packet loss.

# R15. Suppose Host A sends two TCP segments back to back to Host B over a  TCP connection. The first segment has sequence number 90; the second has  sequence number 110. a. How much data is in the first segment? b. Suppose that the first segment is lost but the second segment arrives at  B. In the acknowledgment that Host B sends to Host A, what will be the  acknowledgment number?

There are 20 bytes in the first segment because the segment ranges from bytes 90-109 of the transmission, with the second segment picking it up at byte 110. Since TCP provides cumulative acknowledgments, where the acknowledgment number sent will always be for the earliest missing packet, the acknowledgment number B sends will be 90.

# P3. UDP and TCP use 1s complement for their checksums. Suppose you have the following three 8-bit bytes: 01010011, 01100110, 01110100. What is the 1s complement of the sum of these 8-bit bytes? (Note that although UDP and TCP use 16-bit words in computing the checksum, for this problem you are being asked to consider 8-bit sums.) Show all work. Why is it that UDP takes the 1s complement of the sum; that is, why not just use the sum? With the 1s complement scheme, how does the receiver detect errors? Is it possible that a 1-bit error will go undetected? How about a 2-bit error?

My work is included in a separate file. UDP takes the 1s complement of the sum because when all the words are added with their sum, the result should be a word composed of all 1s, making it easy to spot errors. If any of the 1s are 0s, it denotes an error. It is possible for 1- or 2-bit errors to pass through error checks to the receiver because segments can be corrupted in transport even after they have been checked at the UDP level, and the error checking between the initial check and the destination is not as reliable. However, since processes that use UDP for data transfer do not rely on perfect packet transmission, a couple of errors in transport are not a huge issue.
