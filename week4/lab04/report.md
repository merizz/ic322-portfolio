# Week 4 Lab Report: TCP with Wireshark

# Introduction

This lab was meant to familiarize someone with how TCP data transfers happen and what they look like from not only a networking perspective, but from a mathematical perspective. The biggest thing I learned was that packets are not necessarily ACKed in order, even between all the data coming in. It may have been a Wireshark quirk, but I found it difficult to connect sent packets to their ACKs.

# Process

The lab was pretty simple to follow, save for some missing data and mistakes that set me back. I downloaded the "alice.txt" file from the specified site, picked it from my filesystem using the "Browse" button on the other website, and started Wireshark. I then uploaded the file to the site, waited a bit, and realized after waiting that I could have stopped the capture after pressing the button to upload the file (mistake #1). The rest of the lab was just combing through the packet capture and answering the below questions. The other big mistake I made was thinking the initial packet was the actual HTTP POST when I had to look for text in the actual packet data that said "POST." I only discovered this was the case once I scrolled to the side in my Wireshark window and saw the ASCII characters next to the hexdump. I thought the version of Wireshark I had downloaded only displayed the hexadecimal data for whatever reason. Another thing that brought me some pain was searching for the ACK to the second data packet. I found the first ACK, but the frame numbers skipped over the second one. I poured over the Wireshark data for about thrity minutes trying to find one ACK packet and eventually gave up on it after finding nothing. I chalked it up to Wireshark somehow missing a few ACKs.

# Questions

# 1. What is the IP address and TCP port number used by the client computer (source) that is transferring the alice.txt file to gaia.cs.umass.edu?  To answer this question, it’s probably easiest to select an HTTP message and explore the details of the TCP packet used to carry this HTTP message, using the “details of the selected packet header window” (refer to Figure 2 in the “Getting Started with Wireshark” Lab if you’re uncertain about the Wireshark windows).

The IP address of my computer being used to transfer the file was 10.25.134.133, and it was doing it over port 62260.

# 2. What is the IP address of gaia.cs.umass.edu? On what port number is it sending and receiving TCP segments for this connection?

The IP address of the site is 128.119.245.12, and the port it had open was 80. I found the IP addresses in the frame data and the ports in the individual packets.

# 3. What is the sequence number of the TCP SYN segment that is used to initiate the TCP connection between the client computer and gaia.cs.umass.edu? (Note: this is the “raw” sequence number carried in the TCP segment itself; it is NOT the packet # in the “No.” column in the Wireshark window.  Remember there is no such thing as a “packet number” in TCP or UDP; as you know, there are sequence numbers in TCP and that’s what we’re after here.  Also note that this is not the relative sequence number with respect to the starting sequence number of this TCP session.). What is it in this TCP segment that identifies the segment as a SYN segment? Will the TCP receiver in this session be able to use Selective Acknowledgments (allowing TCP to function a bit more like a “selective repeat” receiver, see section 3.4.5 in the text)?

Digging into the SYN segment, the raw sequence number was 276107917. I knew it was the SYN segment not only because of the flag but because the relative sequence number was zero. The receiver should be able to use selective ACKs because the window size was specified in the packet data.

# 4. What is the sequence number of the SYNACK segment sent by gaia.cs.umass.edu to the client computer in reply to the SYN? What is it in the segment that identifies the segment as a SYNACK segment? What is the value of the Acknowledgement field in the SYNACK segment? How did gaia.cs.umass.edu determine that value? 

The raw sequence number for this statement was 696940234. This statement was labeled with a flag of the same name as well as having both a sequence number of 0 and a relative ACK number of 1. The raw ACK number was 276107918 because that is the next expected byte in the transmission.

# 5. What is the sequence number of the TCP segment containing the header of the HTTP POST command?  Note that in order to find the POST message header, you’ll need to dig into the packet content field at the bottom of the Wireshark window, looking for a segment with the ASCII text “POST” within its DATA field1,2.  How many bytes of data are contained in the payload (data) field of this TCP segment? Did all of the data in the transferred file alice.txt fit into this single segment?

As expected, the sequence number of the first packet (the first transmission after the SYN ACK), was 276107918. The first segment was 714 bytes, and given that the file is over 15,000 bytes long, the segment did not contain all the file data. The file was broken into 128 segments.
# 6. Consider the TCP segment containing the HTTP “POST” as the first segment in the data transfer part of the TCP connection. At what time was the first segment (the one containing the HTTP POST) in the data-transfer part of the TCP connection sent? At what time was the ACK for this first data-containing segment received? What is the RTT for this first data-containing segment? What is the RTT value the second data-carrying TCP segment and its ACK? What is the EstimatedRTT value (see Section 3.5.3, in the text) after the ACK for the second data-carrying segment is received? Assume that in making this calculation after the received of the ACK for the second segment, that the initial value of EstimatedRTT is equal to the measured RTT for the first segment, and then is computed using the EstimatedRTT equation on page 242, and a value of a = 0.125.

According to packet data, the first segment, frame 38, was sent at 11:09:05.37708 AM and was ACKed at 11:09:05.403715 AM. It had an RTT of .026633 seconds. As I mentioned before, I could not find the ACK for the second segment, but I know the estimated RTT was around 43 milliseconds for both segments based on performing the specified graphical analysis. 

# 7. What is the length (header plus payload) of each of the first four data-carrying TCP segments?

Adding the header size of 20 bytes to each payload, the totals are 734, 1220, 1220, and 1220, respectively.

# 8. What is the minimum amount of available buffer space advertised to the client by gaia.cs.umass.edu among these first four data-carrying TCP segments1?  Does the lack of receiver buffer space ever throttle the sender for these first four data-carrying segments?

Going based on the window size, the available buffer space was 131840 bytes. It did not seem like the sender was initially throttled, at least; there were no ACKs that came through in between any of these transmissions.

# 9. Are there any retransmitted segments in the trace file? What did you check for (in the trace) in order to answer this question?

There were no apparent retransmissions for this upload; Wireshark turns retransmitted packets black so that they stand out. I found some retransmissions for a different data transfer in my capture.

# 10. How much data does the receiver typically acknowledge in an ACK among the first ten data-carrying segments sent from the client to gaia.cs.umass.edu?  Can you identify cases where the receiver is ACKing every other received segment (see Table 3.2 in the text) among these first ten data-carrying segments?

As per my earlier observations, the ACKs skipped a lot in the beginning of the stream; for example, the first, second, and third ACKs were for the first, fifth, and tenth segments.

# 11. What is the throughput (bytes transferred per unit time) for the TCP connection?  Explain how you calculated this value.

The throughput was about 6,250 bits per second; I picked one packet and found a "Throughput" graph option under "Statistics." (file: throughput.pdf)

# 12. Use the Time-Sequence-Graph(Stevens) plotting tool to view the sequence number versus time plot of segments being sent from the client to the gaia.cs.umass.edu server.  Consider the “fleets” of packets sent around t = 0.025, t = 0.053, t = 0.082 and t = 0.1. Comment on whether this looks as if TCP is in its slow start phase, congestion avoidance phase or some other phase. Figure 6 shows a slightly different view of this data.

It looks like congestion avoidance because it seems like the packets in each grouping are being buffered at a specific time during the transmission. (file: theirnums.png)

# 13. These “fleets” of segments appear to have some periodicity. What can you say about the period?

They occur about every .025 seconds.

# 14. Answer each of two questions above for the trace that you have gathered when you transferred a file from your computer to gaia.cs.umass.edu

For my data, it looks like more of a slow start into one sudden uptick of transmissions. The uptick happens about 5 seconds in. (file: sequencenums.pdf)
