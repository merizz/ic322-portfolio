# Week 4 Learning Objectives

# 1. I can explain how TCP and UDP multiplex messages between processes using sockets.

The main objective of TCP and UDP is to take IP delivery from working between two systems to between two processes on those systems. These protocols accomplish this through transport-layer multiplexing and demultiplexing. That system sues sockets as doors to get from the sending process to the network and from the network to the receiving process. Demultiplexing comes in when data is approaching a receiving socket; this process directs the segment through the correct socket. On the sender side, multiplexing is how data is gathered into segments and passed into the network. To indicate which socket each segment belongs to, the segments are assigned source port numbers and destination port numbers.

In the case of UDP, a port number is assigned to a single socket that receives all data segments. The role of the source port number then becomes that of a return address rather than an indicator of a specific destination port; if the receiver wants to send something back to that source, it looks at the source port. A host may have many of these UDP sockets running, but only one is connected to a specific sender or receiver.

The difference between UDP and TCP in multiplexing is that there are multiple TCP sockets per connection that accept each data segment. It also has two more data fields than UDP's destination port and IP that it stores: source port and source IP.

# 2. I can explain the difference between TCP and UDP, including the services they provide and scenarios each is better suited to.

UDP (User Datagram Protocol) offers as little as possible in the way of transport protocols. It is almost like interfacing directly with the IPs of the source and destination; it adds fields necessary for the multiplexing process described above and sends the data away. There is no handshake between sender and receiver, making it connectionless. TCP is typically a more preferable connection, but UDP is useful in the following ways:

 - UDP immediately packages and sends the data as soon as the sending process passes it up. This makes UDP generally faster than TCP which waits for acknowledgment from the receiver before sending/halts the sender whenever there is some sort of data loss or corruption.

 - Relating to the previous point, there is no three-way handshake to UDP, so there is no connection establishment delay.

 - UDP does not keep track of connection state, as it is not as preoccupied with reliable transfer. This means an application running UDP can handle more clients at a time.

 - UDP has only 8 bytes of overhead per segment versus TCP which has 20.

Some services provided by UDP are HTTP/3, remote file transfer, Internet telephony, network management, and name translation.

TCP (Transmission Control Protocol) is much more deliberate and reliable when it comes to data transfer. The sender and receiver must handshake before the data transfer happens, sending each other segments with the parameters of the transfer. It includes more features than UDP to strengthen its transfers, like error detection, retransmissions, cumulative acknowledgments, timers, and more header fields. Instead of managing one connection at a time, TCP has full-duplex service in which it can manage flow in two directions. Connections are also point-to-point, meaning one sender and one receiver are involved rather than one sender and a bunch of receivers. Some applications of TCP are electronic mail, different types of remote terminal access, HTTP, file transfer, multimedia streaming, and Internet telephony.

# 3. I can explain how and why the following mechanisms are used and which are used in TCP: sequence numbers, duplicate ACKs, timers, pipelining, go-back-N, selective repeat, sliding window.

Sequence numbers are part of a mod-2 numbering system to order packets so that the receiving host can confirm it has everything in sequence. If the receiver gets two packets in a row with the same number, it is an indication that the previous packet was corrupted and should be replaced by the new packet. If a packet comes in with the next number in the sequence, the receiver knows to collect it without replacing. Sequence numbers can also help identify dropped or out-of-order packets; if a packet with a sequence number two or more values ahead of where it should be arrives, the receiver can throttle the sender until the missing packet is sent. Duplicates can also be identified and weeded out.

Duplicate ACKs are two or more ACKs sent for one packet by the receiver and are an indication that the packet being ACKed was not sent correctly. This can help identify corrupted packets and signal the sender to re-send the packet. 

Timers are countdowns implemented by the sending host once they transmit a packet to help identify dropped packets. If the packet does not reach its destination within the amount of time it takes to make a round-trip and be processed at the receiver, the packet is sent again by the sender. If the original packet does manage to get to the receiver and a duplicate exists, it is handled accordingly.

Pipelining is the process of sending multiple packets across the network without waiting for an acknowledgment of receipt for each one. The problem that it solves is a lack of speed; transmissions can happen many times more quickly with pipelining in place. However, it does introduce a few nuances. There must be a greater range of sequence numbers because each individual packet still needs one, buffering should be used to hold unprocessed packets in a queue, and go-back-N/selective repeat are useful for preventing transmission errors.

Go-back-N puts a constraint on how many unacknowledged packets can be sent per transmission when pipelining is implemented. There are four categories this system organizes packets into: those that are transmitted and acknowledged, transmitted but not acknowledged, those that can be transmitted once data arrives, and those that cannot be used until a currently unacknowledged packet has been acknowledged. Since the constraining number is considered a window size, go-back-N is a sliding-window protocol. There are three types of events the sender in a go-back-N system has to react to:

 - If there are N unacknowledged packets in the window, the sender interfaces with the application layer to pause further transmissions. If it is not full, the sender allows a packet through.

 - The receipt of ACKs is tracked.

 - If there is a timeout indicating a packet was lost, the sender re-sends all of the previously-unacknowledged packets.

The receiver either sends and ACK for an in-order packet or discards the packet. This eliminates out-of-order or duplicate packets.

Selective repeat attempts to eliminate all the unneccesary retransmissions of packets that plague go-back-N. It makes the sender re-send only the packets that may have been corrupted. It does this by having the receiver individually acknowledge each packet that is correctly received. Unlike go-back-N, selective repeat buffers out-of-order packets until all packets are received in the correct order instead of discarding them. The sender has the following responsibilities:

 - If the data is for the next packet in the sequence, the packet is sent. If not, it is buffered or sent back to the application.

 - Each packet has its own timer, differing from go-back-N, but the sender still controls timing to check for packet loss.

 - When an ACK is received for a packet, the window moves to the next un-ACKed packet. Any untransmitted packets in that new window are transmitted.

The receiver does the following:

 - When a packet is received in order, an ACK is sent to the sender. If it was not received before, it is buffered, and if the packet is at the base of the window, the packet and those that came before it in order are sent to the application layer.

 - A packet can be acknowledged twice.

 - All other packet situations are ignored.

TCP uses most of the tools discussed above, namely sequence numbers, cumulative acknowledgments, timers, and some type of pipelining.
  
# 4. I can explain how these mechanisms are used to solve dropped packets, out-of-order packets, corrupted/dropped acknowledgements, and duplicate packets/acknowledgements.

This is answered in the above section.
