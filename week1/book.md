# Week 1 Questions

# R1. What is the difference between a host and an end system? List several different types of end systems. Is a Web server an end system?

A host and an end system are essentially the same thing and can be used in place of one another according to the curriculum. A host/end system is some interface between the Internet and the user, using a network connection to run apps. Some hosts I have include my Echo Dot, my phone, my USNA computer, my tablet, and my Apple Watch. A Web server also counts as a host because it completes the aforementioned function of a host and is a host subset according to the book (along with clients).

# R4. List four access technologies. Classify each one as home access, enterprise access, or wide-area wireless access.

3G: wide-area
DSL: home
Ethernet: home and enterprise
WiFi: home and enterprise

# R11. Suppose there is exactly one packet switch between a sending host and a receiving host. The transmission rates between the sending host and the switch and between the switch and the receiving host are R1 and R2, respectively. Assuming that the switch uses store-and-forward packet switching, what is the total end-to-end delay to send a packet of length L?

If it takes R1 b/s to get from the sending host to the switch and R2 b/s to get from the switch to the receiving host, and the formula for transmission delay is packet length divided by transmission rate, the total end-to-end delay for this exchange is L/R1 + L/R2.

# R12. What advantage does a circuit-switched network have over a packet-switched network? What advantages does TDM have over FDM in a circuit-switched network?

If you're using a circuit-switched network, you have all the resources on that network to yourself, and packets are sent/received at a constant rate. TDM doesn't have to divide the bandwidth like FDM does, so it presumably requires less energy to use.

# R13. Suppose users share a 2 Mbps link. Also suppose each user transmits continuously at 1 Mbps when transmitting, but each user transmits only 20 percent of the time. When circuit switching is used, how many users can be supported?

Since the link is 2 Mbps, and each user transmits at 1 Mbps, 2 users can be supported.

b. For the remainder of this problem, suppose packet switching is used. Why will there be essentially no queuing delay before the link if two or fewer users transmit at the same time? Why will there be a queuing delay if three users transmit at the same time?

In the above example, two hypothetical users would not be going over the allotted bandwidth because the transmission adds up to 2 Mbps. If there are three users transmitting, the transmission would exceed the bandwidth by 1 Mbps.

c. Find the probability that a given user is transmitting.

The problem states that each user transmits 20% of the time.

d. Suppose now there are three users. Find the probability that at any given time, all three users are transmitting simultaneously. Find the fraction of time during which the queue grows.

Multiplying each user's transmission percentage together results in a .8% transmission rate for three users. Theoretically, the fraction representing the queue growth would be equal to this percentage (1/125).

# R14. Why will two ISPs at the same level of the hierarchy often peer with each other? How does an IXP earn money?

It is less expensive for peering ISPs to share traffic because they would otherwise have to pay ISPs that are higher in the hierarchy to carry their traffic. Since IXPs are places typically offered by high-tier ISPs to lower-tier ISPs for peering, they make money because they are technically managed by those higher ISPs.

# R18. How long does it take a packet of length 1,000 bytes to propagate over a link of distance 2,500 km, propagation speed 2.5 * 10^8 m/s, and transmission rate 2 Mbps? More generally, how long does it take a packet of length L to propagate over a link of distance d, propagation speed s, and transmission rate R bps? Does this delay depend on packet length? Does this delay depend on transmission rate?

Transmission delay = packet length/transmission rate

Propagation delay = link distance/propagation speed

Therefore, transmission time is equal to these delays combined: L/R + d/s

For the problem presented, this is 1,000 bytes/2 Mbps + 2,500 km/2.5 * 10^8 m/s.

1 byte = 1 * 10^-6 Mb

.001 Mb/2 Mbps = .0005 s

2,500,000 m/2.5 * 10^8 m/s = .01 s

.0005 + .01 = .0105 s

Delay depends on both packet length and transmission rate.

# R19. Suppose Host A wants to send a large file to Host B. The path from Host A to Host B has three links, of rates R1 = 500 kbps, R2 = 2 Mbps, and R3 = 1 Mbps. Assuming no other traffic in the network, what is the throughput for the file transfer?

Since R1 is the limiting transmission rate, the throughput for this system is 500 kbps.

b. Suppose the file is 4 million bytes. Dividing the file size by the throughput, roughly how long will it take to transfer the file to Host B?

4 * 10^6 b/500 * 10^3 b = 8 s

c. Repeat (a) and (b), but now with R2 reduced to 100 kbps.

Throughput: now 100 kbps

4 * 10^6 b/100 * 10^3 b = 40 s
