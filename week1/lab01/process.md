# Process

# 1. Open Wireshark

The first thing I did was download the Wireshark application for Windows and open it up. That was pretty straightforward.

# 2. Prepare the specific URL to be used for HTTP request capture

I copied the URL "http://gaia.cs.umass.edu/wireshark-labs/INTRO-wireshark-file1.html" into a fresh Firefox window, being careful not to hit "enter" until I completed the folowing step.

# 3. Start capturing with Wireshark

I went back into Wireshark after prepping the URL, chose "WiFi" as my capture interface, and hit the blue fin in the top left-hand corner to begin capture. I then went into Firefox again and hit "enter" with the URL in the search bar, presumably completing a GET request. The request did not show up in my packet feed initially (maybe ITSD blocked it?), but I saw a GET request and a response to that request once I refreshed the Firefox page. I stopped my capture with the HTTP packets in my feed.

# 4. Analyze

I was able to filter out the desired packets and perform analysis on the data that came back. The details of that analysis are included in the "Questions" section.
