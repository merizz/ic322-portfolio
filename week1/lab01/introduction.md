# Introduction

This is a lab in which I brushed up on my Wireshark skills and specifically analyzed what loading a website looks like in network packet form. I understood most of what happened already, but I did learn the difference between data collected from loading a website and that of refreshing a website. It takes more time and communication with the server to load the page while the server simply confirms no changes were made to the site when it is refreshed.
