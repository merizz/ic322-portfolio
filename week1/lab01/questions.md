# Questions

# 1. Which of the following protocols are shown as appearing in your trace file: TCP, QUIC, HTTP, DNS, UDP, TLSv1.2?

My trace file included TCP, HTTP, and TLSv1.2 packets.

# 2. How long did it take from when the HTTP GET message was sent until the HTTP OK reply was received?

It took .022462 seconds.

# 3. What is the Internet address of the gaia.cs.umass.edu? What is the Internet address of your computer or the computer that sent the HTTP GET message?

The source address is 10.25.134.133, and the destination address is 128.119.245.12.

# 4. What type of web browser issued the HTTP request?

I used Firefox. The string said "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/116.0\r\n."

# 5. What is the destination port number to which this HTTP request is being sent?

The request was sent through port 80.
