# Week 1 Learning Objectives

# 1. I can explain the role that the network core plays vs. the network edge.

The network core makes up the communication infrastructure of all connected machines. It consists of ISPs, routers, switches, wires, wireless connection, and any other harware or software means of getting data from one place to another. It is the large, overarching connection scheme. 

Meanwhile, the network edge is made up of all the end systems and/or hosts that are involved in the network as well as the local connections that merge them with the network core. These are what use the data provided through the network core to run applications and interface with users.

# 2. I can compare different physical technologies used in access networks, including dial-up, DSL, cable, fiber, and wireless.

Dial-up was the original way of connecting to the Internet. It relied on phone lines and service to connect users. This means that if someone were using the phone, nobody could use the Internet, and vice-versa. It also could spell trouble if someone were in the middle of downloading software and a phone call came through.

DSL was an attempt to remedy this issue with dial-up, splitting the phone lines and the internet data. It used DSLAM hardware to accomplish this, and every home had consistent bandwidth because data was not shared (it depended upon individual phone service providers).

Cable came next and exapnded upon the idea of DSL by splitting connection between TV signal and the Internet rather than with phone data. TV providers naturally took control of this responsibility. DSLAM was replaced by CMTS, and copper wires were replaced by fiber cables. The connection was faster because of the fiber cables used to facilitate it, but unlike with DSL, the bandwidth was shared with other people using that TV provider.

Finally, Internet companies became a reality and took the fast fiber-optic connection idea from TV companies to give everyone the reliable bandwidth that DSL once provided. They also got rid of the separation hardware and cable-switching that came with their predecessors, facilitating a single fiber connection from each house to the provider. 

There are three main forms of wireless connection: WiFi, cellular service, and satellite service. WiFi is present within buildings and connects people to the Internet through a router that passes that connection to modems provided by Internet companies. Cellular services, like 3G and LTE 4G and 5G, facilitate connection through cell towers placed all around the world. If you're close enough to a tower and unable to access WiFi, cellular service can connect you to the Internet. Finally, satellites do what cell towers cannot by allowing for connection from space. They are typically very expensive to launch, but Starlink is attempting to fix this issue through its cost-effectiveness (these satellites are launched in low-earth orbit).

# 3. I can use queuing, transmission, and propagation delays to calculate total packet delay, I can decsribe the underlying causes for these delays, and I can propose ways to improve packet delay on a network.

To calculate total packet delay, you must add queuing delay, transmission delay, and propagation delay for each packet involved in a transmission. Transmission delay is calculated by dividing packet length by transmission rate. Propagation delay is found by dividing distance between routers and propagation speed. Queuing delay for packets after the first is calculated by adding the other two delays from the previous packet(s).

Transmission delay is caused by the time it takes to get the packet from the router to the wire. It can be improved by implementing faster hardware. Propagation delay is caused by how fast (or slow) the packet moves along the wire to the next router. This can be improved by using the fastest possible wires and/or shortening those wires. Queuing delay is caused by the buildup of multiple packets that must be transmitted at once. It can be improved through any of the aforementioned means or by creating multiple routes to the same destination within the network.

# 4. I can describe the differences between packet-switched networks and circuit-switched networks.

Circuit-switched networks are nice if you need guaranteed connection and bandwidth, but it does not allow for other users to have that same guarantee, so it's a clunky way of doing things. Packet-switched networks divide up the bandwidth, meaning the entirety of a message may not get through immediately, but multiple people can start sending things at the same time.

# 5. I can describe how to create multiple channels in a single medium using FDM and TDM.

Time Division Multiplexing (TDM) creates multiple channels by assigning each host a time slot to transmit in. Frequency Division Multiplexing (FDM) gives each host a band of frequency to transmit on rather than a time slot.

# 6. I can describe the hierarchy of ISPs and how ISPs at different or similar levels interact.

Local ISPs are the first link from users to the Internet. Regional ISPs then sell their connection to the local ISPs and propagate it to the Tier 1 ISPs. These are essentially the gateway to the Internet. Lower-tier ISPspeer together and share their traffic to avoid higher costs of sending traffic directly through Tier 1 ISPs. Higher ISPs also offer IXPs through which the smaller ISPs can peer for a lesser rental fee.

# 7. I can explain how encapsulation is used to implement the layered model of the Internet.

There are five layers in the layered Internet model: The physical layer (1), the link layer (2), the network layer (3), the transport layer (4), and the application layer (5). Each layer has its own way of encapsulating data and identifying where it needs to go; these identifiers are added as data is sent from an application down to the physical layer, and they are stripped on the way to the receiving host. Layer 5 is the raw data, layer 4 adds a UDP header, layer 3 adds an IP header, layer 2 adds a frame header/footer, and layer 1 sends the packet through physical means to the location of the receiving host where it will travel back up the Internet layers. 
