# Week 7 Lab Report

# Introduction

This lab was meant to teach the basics of networking through Python and was also a way for us to Beta-test the Protocol Pioneer game. Things I was refreshed on were how to split strings and search for substrings.

# Process

For chapter 3, I first split the message by newline characters, taking each element of that list and splitting it by colons. I loaded the values from the right side of each colon into variables I could use to run the functions and send the messages along. The logic I used to forward messages withthe drones can be seen in my solution file; I essentially made a bunch of conditional statements.

For chapter 4, I handled the parsing aspect at the scanner objects, using only the drones to forward messages and carry the responses back to their respective analyzers. The main hiccup I ran into was figuring out how to make an algorithm that could both receive and forward messages. It took a lot of specific substring searching to get it right. I followed similar logic to that of the third chapter for parsing data.

# Testing Notes

I did not find any bugs, and I thought it was much easier to understand the goal of each chapter; I had a clearer idea of what I was supposed to do than I had last round.

