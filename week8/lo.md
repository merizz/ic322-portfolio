# Week 8 Learning Objectives

# 1. I can explain the problem that NAT solves, and how it solves that problem.

Network Address Translation (NAT) solves the problem of not having enough allocated space for IP addresses on a private network. Using NAT, ISPs only need to allocate a single IP to private entities that will cover all of their devices. The addresses provided are secure from the outside world, and each one can easily be changed.

In order for NAT to properly work, the router connected to the private subnet must replace any private IP sending information out with a public IP. From the moment the datagram leaves the subnet, the public IP associated with the private network is used in any further communications with outside entities. The router keeps public and private IPs straight using a NAT table. In the table, the public IP and port are listed in one column while the private IP and port are listed in the opposite column. There are a few drawbacks to using NAT, such as insecure end-to-end communication with the change in port number at the router, but it helps to conserve IP address space in conjunction with IPv6.

# 2. I can explain important differences between IPv4 and IPv6.

IPv6 addresses are slightly different in datagram format from IPv4. The most noticeable difference is that these new addresses, which were made to provide new space after the 32-bit IPv4 addresses were all taken, are 128 bits in size. They also have a priority field to store the priority of the packet and a flow field to group packets within the same "flow" together. Just as there are new components, there are also components that were eliminated. There is no checksum to make processing faster, packets are not fragmented and reassembled, and options are handled at another layer rather than at the IP level.

# 3. I can explain how IPv6 datagrams can be sent over networks that only support IPv4.

Tunneling is the process by which IPv6 datagrams can be sent through routers that have not been updated from IPv4. In tunneling, an IPv6 datagram is packaged within an IPv4 packet so it is compatible with the router. The entire datagram is placed in the data field of the IPv4 packet, sent through any IPv4 routers between two IPv6 routers (called a "tunnel"), and unpackaged on the other end. Once the datagram reaches another IPv6 router, it continues as an IPv6 datagram until it either reaches its destination or hits another IPv4 router. 

