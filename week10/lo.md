# Week 10 Learning Objectives

# 1. I can explain what autonomous systems are and their significance to the Internet.

Autonomous systems (AS) are essentially groups of routers. They are also known as domains. There are two different types of communication that ASs accomplish: intra-AS and inter-AS communication. Intra-AS routing is sending information between routers that are within the same AS. All routers participating in this communication have to use the same protocol, and a gateway router can initiate communication with other ASs. Outside communication is called inter-AS communication. 

Forwarding tables within routers are created by these types of communication. Intra-AS creates entries for the local network, and entries for other networks are made in coordination with inter-AS. The most common intra-AS routing protocols are RIP (Routing Information Protocol), EIGRP (Enhanced Interior Gateway Routing Protocol), and OSPF (Open Shortest Path First). The protocol used for inter-AS is BGP (Border Gateway Protocol).

# 2. I can describe how the BGP protocol works as well as why and where it is used.

BGP allows a domain to be visible to the rest of the Internet, telling other networks what destinations it is able to reach. It is a type of inter-AS communication. There are two different types of BGP: eBGP and iBGP. eBGP allows a network to get information on whether or not it can reach another network from networks around it, and iBGP allows the network to broadcast what it knows about reachability to its neighbors.

In a BGP session, two routers will send each other the paths they know about over a TCP connection. Each router advertises the path it will send data over using a prefix and attributes. The prefix is the destination the router is sending the datagram to. Here are two examples of attributes:

 - AS-PATH: a list of other networks the datagram has traveled through
 - NEXT-HOP: the next specific router within an AS that a datagram will be sent to

Each AS has a policy in place on whether to accept, reject, and/or advertise a path between routers. Import policy refers to the decision to accept or reject a path.

BGP facilitates messaging between peers to determine what needs to be done over the connection. Here are some examples of BGP messages:

 - OPEN: opens and establishes a TCP connection with a BGP peer
 - UPDATE: advertise new path or get rid of an old one 
 - KEEPALIVE: keeps the connection going regardless of activity and/or ACKs an OPEN message 
 - NOTIFICATION: reports errors and/or closes the connection

BGP selects routes for datagrams based on criteria such as the following:

 - policy decision
 - shortest path
 - closest NEXT-HOP router

# 3. I can explain what the ICMP protocol is used for, with concrete examples.

ICMP (Internet Control Message Protocol) is most often used for error reporting. An example outlined in the book is an error message reading "Destination network unreachable" sent during an HTTP session. This message was an ICMP message sent from a router that could not find the path to the requested host or object.

The program "ping" also uses ICMP. The program sends an echo request (type 8) to the target host, and the host sends back an echo reply (type 0) if it is reachable. Another program that uses ICMP is traceroute. When the TTL of a packet expires, a warning message (type 11) is sent to the source, including the router's name and IP address. If the program is sending UDP packets, it knows when the packet has reached its destination when an ICMP type 3 message (port unreachable) is returned.
