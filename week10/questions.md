# Week 10 Questions

# R11: How Does BGP use the NEXT-HOP attribute? How does it use the AS-PATH attribute?

AS_PATH is used to prevent duplicate advertisements from being sent and to choose which route to a prefix is best. NEXT-HOP helps to build forwarding tables, as it denotes the next router along a path to a prefix. 

# R13: True or False: When a BGP router recieves an advertised path from its neighbor, it must add its own identity to the received path and then send that new path on to all of its neighbors. Explain.

This is false; since BGP has policies in place that help it choose paths between routers, it has the option to send its identity, not the obligation.

# P19: Names four different types of ICMP messges. **DOWNS:** Also describe what they are used for.

 - type 0: echo reply (what a target host sends to you in ping)
 - type 3: destination network unreachable (an example would be a router not being able to resolve a path to an HTTP host)
 - type 8: echo request (what ping sends to a target host)
 - type 11: TTL expired (this tells traceroute that a datagram expired and should probably be resent)

# P20: What two types of ICMP messages are received at the sending host executing the traceroute program?

Type 11 messages are sent to the sending host when a packet expires, and a type 3 message is sent to a UDP sender when the last packet has made it to its destination.

# P14: Consider the network shown below (in the book). Supose AS3 and AS2 are running OSPF for their intra-AS routing protocol.Suppose AS1 and AS4 are running RIP for their intra-AS routing protocol.Suppose eBDO and iBGP are used for the inter-AS prouting proces. Initially suppes there is no physical link between AS2 A2 a.
## a. Router 3c learns about prefix x from which routing protocol: OSPF, RIP, eBGP， iBGP?

It learns about it from eBGP because it is communicating through inter-AS.

## b. Router 3a learns about x from which routing protocol？

This is also inter-AS communication, so the protocol is iBGP.

## c. Router 1c learns about x from which routing protocol？

Just like router 3c, router 1c gets its info from eBGP.

## d. Router 1d learns about x from which routing protocol？

Just like router 3a, router 1d gets its info from iBGP.

# P15: Referring to the previous problem, once a router 1d learns about x it will put an entry (x,I) in its forwarding table.
## a. Will I be equal to I1 or I2 for this entry? Explain why in once sentence.

The least cost path to the destination is through I1, so that is what I will be equal to.

## b. Now suppose that there is a physical link between AS2 and AS4, shown by the dotted line. Suppose router 1d learns that x is accessible via AS2 as well as via AS3. Will I be sent to I1 or I2? Explain why in one sentence.

I2 has the closest router that is the next hop away, so I will be equal to I2.

## c. Now suppose there is another AS, called AS5, which lies on the path between AS2 and AS4 (not shown in diagram). Suppose router 1d learns that x is accessible via AS2 AS5 AS4 as well as via AS3 AS4. Will I be set to I1 or I2? Explain why in one sentence.

I1 is the start of the shortest AS-PATH, so I will be equal to I1.

# P19: In Figure 5.13, suppose that there is another stub network V that is a customer of ISP A. Suppose that B and C have a peering relationship, and A is a customer of both B and C. Suppose that A would like to have the traffic destined to W to come from B only, and the traffic destined to V from either B or C. How should A advertise its routes to B and C? What AS routes does C receive?

A should advertise the paths A-W and A-V to B as well as path A-V to C. C will receive paths B-A-W, B-A-V, and A-V.

# P20: Suppose ASs X and Z are not directly connected but instead are connected by AS Y. Further suppose that X has a peering agreement with Y, and that Y has a peering agreement with Z. Finally, suppose that Z wants to transit all of Y's traffic but does not want to transits X's traffic. Does BGP allow Z to implement this policy?

If X peers with Y and Y peers with Z, X has a way of peering with Z. BGP can get reachability info from neighboring networks, so it will allow development of the policy.
