# Week 15 Learning Objectives

# 1. I can explain how two strangers are able to exchange secret keys in a public medium.

The Diffie-Hellman Key Exchange was the foundation of public key encryption, which preserves the integrity of keys sent over a public medium. 

![Key Exchange](keys.png)

In this example, Sylvie wants to send Loki a message. Loki has two different keys: a public key that is available to anyone and a private key that is unique to him. To send her message, Sylvie retrieves Loki's public key and encrypts her message using that key and some sort of encryption algorithm. To decrypt Sylvie's message, Loki uses his private key and the corresponding decryption algorithm. 

Unfortunately, Ravonna Renslayer wants to intercept Sylvie's communication with Loki so she can figure out where Sylvie is and bring her into the Time Variance Authority for the crime of her existence. To help prevent this from happening, Sylvie uses the RSA algorithm to encrypt her messages. This algorithm uses modulo arithmetic with extremely large prime numbers, making it very difficult to brute-force crack in a reasonable amount of time. Putting everything together, Sylvie first encrypts her message to Loki using an RSA session key which she gives to him; this is a type of symmetric key. The session key is encrypted using Loki's public key just as the message would have been in the first scenario.

Two other ways Sylvie can make sure her message gets to Loki intact without being intercepted are using a message authentication code (MAC) and/or a digital signature. To use a MAC, Sylvie would tack an authentication key, shared with Loki, on to her message, hashing both of them together. The MAC is attached to the resulting hash and sent to Loki. Since Loki knows the authentication key, he can calculate the hash and verify the message's integrity. To use a digital signature, Sylvie would apply her private key to the hash of her message, and Loki could use her public key to undo the signature, obtaining the hash. Loki would also receive a plaintext version of Sylvie's message; if the hash of the plaintext matches the digitally-signed copy, the integrity of the message has been maintained.

# 2. I can walk through the TLS handshake and explain why each step is necessary.

TLS, or transport layer security, is a more secure version of TCP used to facilitate key/message exchanges and agree on encryption methods. The latter function of TLS is accomplished through the handshake phase, which has six steps:

 1. The client sends a list of cryptographic algorithms it supports, along with a client nonce. The list of algorithms needs to be sent to the receiver so they know which of the algorithms they support are compatible on the other end. The client nonce is used to create a session key for an extra layer of protection on messages (detailed above).

 2. From the list, the server chooses a symmetric algorithm and a public key algorithm, and HMAC algorithm along with the HMAC keys. It sends back to the client its choices, as well as a certificate and a server nonce. Each type of key has its own role in the exchange, as mentioned in the previous question. The certificate validates the identity of the receiver, and the nonces act as sequence numbers so transmissions are ordered correctly/any tampering with the transmission is evident.

 3. The client verifies the certificate, extracts the server's public key, generates a pre-master secret (PMS), encrypts the PMS with the server's public key, and sends the encrypted PMS to the server. The identity of the receiver is confirmed by the sender by grabbing the public key, and the PMS initiates the calculation of the master secret (MS) that will encrypt the session.

 4. Using the same key derivation function, the client and server independently compute the MS from the PMS and nonces. The MS is then sliced up to generate the two encryption and two HMAC keys. Furthermore, when the chosen symmetric cipher employs CBC, then two initialization vectors (IVs)--one for each side of the connection--are also obtained from the MS. Henceforth, all messages sent between client and server are encrypted and authenticated. CBC is cipher block chaining, in which messages are broken up and encrypted with the help of an IV, which is a string of bits.

 5. The client sends the HMAC of all the handshake messages.

 6. The server sends the HMAC of all the handshake messages.

The last two steps serve as a check for the integrity of the messages on either end. The client compares messages it receives with hashes on its side to make sure nothing has been changed, and vice-versa. 

# 3. I can explain how TLS prevents man-in-the-middle attacks.

During a transmission, TLS breaks up messages into TLS records. Each record has a section with the hashed and encrypted data as well as the HMAC key. These records provide a way to check for message integrity (by comparing hashes) throughout the entire transmission, not just at the beginning or the end. This makes it easier to spot if/when a man-in-the-middle attack occurs.

Sequence numbers also help detect and prevent attacks. They keep track of the order data is supposed to be sent in as well as if data comes through at all. It is evidence of tampering if any numbers associated with transmissions are in the wrong sequence.
