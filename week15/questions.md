# Week 15 Questions

# R20. In the TLS record, there is a field for TLS sequence numbers. True or false?

This is false; the sequence number is included with the hash of the data and the HMAC.

# R21. What is the purpose of the random nonces in the TLS handshake? 

Nonces are used to make session keys and prevent connection replay attacks, where an attacker attempts to resend a message they have captured; this will not work with nonces because they are random with every transmission.

# R22. Suppose an TLS session employs a block cipher with CBC. True or false: The server sends to the client the IV in the clear. 

This is true; the IV is not encrypted and is used to encrypt data in future steps of the block chain algorithm.

# R23. Suppose Bob initiates a TCP connection to Trudy who is pretending to be Alice. During the handshake, Trudy sends Bob Alice’s certificate. In what step of the TLS handshake algorithm will Bob discover that he is not communicating with Alice?

Bob will figure out Trudy is the one communicating with him in step 6 of the handshake; if Trudy does not have Alice's private key, she cannot decrypt the PMS, and the HMAC on the server side will be different because the PMS is used to make the MS which is split to make the HMACs.

# P9. In this problem, we explore the Diffie-Hellman (DH) public-key encryption algorithm, which allows two entities to agree on a shared key. The DH algorithm makes use of a large prime number p and another large number g less than p. Both p and g are made public (so that an attacker would know them). In DH, Alice and Bob each independently choose secret keys, SA and SB, respectively. Alice then computes her public key, TA, by raising g to SA and then taking mod p. Bob similarly computes his own public key TB by raising g to SB and then taking mod p. Alice and Bob then exchange their public keys over the Internet. Alice then calculates the shared secret key S by raising TB to SA and then taking mod p. Similarly, Bob calculates the shared key S′ by raising TA to SB and then taking mod p. 

The answer to this problem is in a separate file in this directory.

## a. Prove that, in general, Alice and Bob obtain the same symmetric key, that is, prove S = S′.



## b. With p = 11 and g = 2, suppose Alice and Bob choose private keys SA = 5 and SB = 12, respectively. Calculate Alice’s and Bob’s public keys, TA and TB. Show all work.



## c. Following up on part (b), now calculate S as the shared symmetric key. Show all work.



## d. Provide a timing diagram that shows how Diffie-Hellman can be attacked by a man-in-the-middle. The timing diagram should have three vertical lines, one for Alice, one for Bob, and one for the attacker Trudy.



# P14. The OSPF routing protocol uses a MAC rather than digital signatures to provide message integrity. Why do you think a MAC was chosen over digital signatures?

OSPF is primarily concerned with making sure messages get to their destination intact; therefore, MACs are a more useful option than digital signatures in this scenario. MACs are used to verify the hashes of data whereas digital signatures are used to verify the identity of the sender.

# P23. Consider the example in Figure 8.28. Suppose Trudy is a woman-in-the-middle, who can insert datagrams into the stream of datagrams going from R1 and R2. As part of a replay attack, Trudy sends a duplicate copy of one of the datagrams sent from R1 to R2. Will R2 decrypt the duplicate datagram and forward it into the branch-office network? If not, describe in detail how R2 detects the duplicate datagram.

The duplicate datagram will not be decrypted or forwarded to the branch-office network; TLS detects replay attacks through sequence numbers. If the datagrams are in the wrong order at some point during the transmission, R2 will detect the duplicate and determine an attack has occurred. Therefore, Trudy's attempt at a replay attack will not work with TLS implemented.
