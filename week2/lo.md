# Week 2 Learning Objectives

# 1. I can explain the HTTP message format, including the common fields.

An HTTP message has three main parts: the request/status line, header lines, and the entity body. In a request message, the first line is the request line. The request line includes the method field, the URL field, and the HTTP version field. GET is an example of a method found in the method field; it is used to request objects from a URL in a browser. Common headers found in the header lines of a request message are "Host," "Connection," "User-Agent," and "Accept-Language". The "Host" field reiterates the hostname for Web proxy caches. The "Connection" field specifies how the host wants the connection handled after the data exchange (this is often filled with "close"). The "User-Agent" field specifies the browser used in the exchange, and the "Accept-Language" field specifies the language the host prefers the object to be sent in. The entity body of a GET request is always empty because no content is being pushed to the client.

The status line of an HTTP response mirrors that of a request; it includes the HTTP version the request sent as well as a status code indicating how the exchange went. There are many different status codes, but here are a few of the common ones:

 - 200 OK: The request was successful.
 - 301 Moved Permanently: The object requested was permanently moved somewhere else (the new location is specified in a "Location" header in the response).
 - 400 Bad Request: An error of some kind occurred.   
 - 404 Not Found: The object does not exist.
 - 505 HTTP Version Not Supported: The HTTP version in the request was not supported by the server.

The header lines in a response message are also similar to those in the request message, except they include "Date," "Last-Modified," "Server" instead of "User-Agent," "Content-Length," and "Content-Type". The length and type headers are essentially primers for the entity body, which contains the requested information this time. 

One further thing to note on HTTP messages is that carriage returns and spaces are very important to how the message is interpreted. For instance, if there are not any header lines in a response, there should still be a carriage return where the header lines would be.

# 2. I can explain the difference between HTTP GET and POST requests and why you would use each.

As noted before, HTTP GET requests are used for retrieving objects specified in the URL of a site. A POST method differs from a GET method in that the entity body is filled rather than enpty. POST requests are used when a user fills in some type of form, such as a search bar. GET requests can perform the same function, but the fields entered are stored in the URL rather than the entity body. They are found after the name of the site, and the fields are delimited by question marks.

# 3. I can explain what CONDITIONAL GET requests are, what they look like, and what problem they solve.

CONDITIONAL GET requests were created to confirm that the object on the client side has not been modified on the server side since a request was made. Before these requests existed, there was a chance that objects accessed by a user would be inaccurate on the client side. These requests look very similar to GET requests but contain one unique header line: "If-Modified-Since". If the object has not been modified since the specified date in this header field, the status returned will be "304 Not Modified". This allows the client cache to use its copy of the requested object because it has confirmed that it matches the server copy.

# 4. I can explain what an HTTP response code is, how they are used, and I can list common codes and their meanings.

As covered in LO1, a response code indicates if the request was successful or not. The common response codes and their meanings are listed under LO1.

# 5. I can explain what cookies are used for and how they are implemented.

Cookies are mechanisms used by websites to track user data. They have four parts: a header line in the HTTP response, a header line in the HTTP request, a file kept on the user's system managed by the user's browser, and a database on the website. Here is an example from problem R12 on how cookies can be used to track purchase history on an e-commerce site:

When a user accesses the site, the resulting HTTP response includes a "Set-cookie:" header that is filled by the user's session identification number. This number is used to index an entry in the server's database at which a log of the user visiting the site is stored. When the browser finds the "Set-cookie" header in the HTTP response, it creates an entry in its cookie file with the server's hostname and the identification number. Since the identification number of the user is stored in the user's browser, every time the user visits the e-commerce site, the HTTP request will contain a line that says "Cookie: *I.D. number*", and the site will be able to find all of that user's activity from its database. This includes the user's purchase history.

# 6. I can explain the significance of HTTP pipelining.

HTTP pipelining through HTTP/2 was created to alleviate the Head of Line (HOL) blocking problem caused by sending Web page data over a single TCP connection. Because of the singular connection that sites using HTTP 1.1 had, smaller objects on a page would have to wait to load until larger ones loaded. HTTP/2 works around this not by adding more TCP connections (which sites using 1.1 still have to do), but by breaking messages into frames that are sent at different times around the incoming HTTP responses. With this type of framing implemented, smaller objects are loaded before larger ones because they take fewer frames to fully render.

Another way HTTP/2 speeds up website rendering is by pushing objects that the client presumably needs to their side without having to request them first. The server can see the dependencies of a site in the HTML base and send them all at once. 

There have been talks of HTTP version 3 as well; this version will likely run on a protocol called QUIC. This protocol will implement message multiplexing, per-stream flow control, and low-latency connection.

# 7. I can explain the significance of CDNs and how they work.

CDNs, or Content Distribution Networks, store copies of content in its servers so users can access those copies rather than going all the way to the main servers of companies that use them. There are two types of CDNs: private CDNs, which are owned by providers themselves, and third-party CDNs, which distribute content for providers but are their own corporate entity. 

There are two main ways in which CDNs set up their servers: using the "Enter Deep" method or the "Bring Home" method. The "Enter Deep" method consists of placing CDN clusters within ISPs that allow them to get as close as possible to users, therefore eliminating as much delay as possible and ensuring throughput. The "Bring Home" method consists of placing CDN clusters within IXPs rather than dispersing them between ISPs. This means less maintenance for providers but slower service for users than with the "Enter Deep" method.

The way CDNs work is through processing DNS queries from the user. The user may query content from Netflix, but Netflix may pawn off that DNS query to a trusted CDN. A new query is sent using the CDN's domain name, and an IP Address is sent from the CDN to the user. A TCP connection between the CDN and the user can then be established.

The cluster of servers that content is pulled from within the CDN is carefully chosen to ensure the best user experience. This is formally called a cluster selection strategy. A typical strategy is using the cluster that is geographically closest to the user. Geographic location is attached to a user's IP address, so the best location to pull from can easily be found on the server side. There are three issues with this: the user may be farther from their assigned cluster in terms of network length, they could be using a VPN, and it doesn't consider loss of bandwidth availability by always assigning a user to the same cluster. A better strategy is using DNS probes to figure out which client-cluster connections will perform the best. This is called "real-time measurement." 
