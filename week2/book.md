# Week 2 Questions

# R5. What information is used by a process running on one host to identify a process running on another host?

In order to identify the specific process a packet needs to be sent to, the source host needs the port number of the destination host. Some popular port numbers are 80 (HTTP), 25 (SMTP), and 22 (SSH).

# R8. List the four broad classes of services that a transport protocol can provide. For each of the service classes, indicate if either UDP or TCP (or both) provides such a service.

 - reliable data transfer: TCP
 - throughput: neither
 - timing: neither
 - security: TCP

The book mentions that although applications that rely on throughput and/or timing exist on the Internet, neither TCP nor UDP can guarantee these services.

# R11. Why do HTTP, SMTP, and IMAP run on top of TCP rather than on UDP?

These application-layer protocols function on reliable data transfer and secure communication, both of which are provided by TCP and not by UDP. 

# R12. Consider an e-commerce site that wants to keep a purchase record for each of its customers. Describe how this can be done with cookies.

When a user accesses the site, the resulting HTTP response includes a "Set-cookie:" header that is filled by the user's session identification number. This number is used to index an entry in the server's database at which a log of the user visiting the site is stored. When the browser finds the "Set-cookie" header in the HTTP response, it creates an entry in its cookie file with the server's hostname and the identification number. Since the identification number of the user is stored in the user's browser, every time the user visits the e-commerce site, the HTTP request will contain a line that says "Cookie: *I.D. number*", and the site will be able to find all of that user's activity from its database. This includes the user's purchase history.

# R13. Describe how Web caching can reduce the delay in receiving a requested object. Will Web caching reduce the delay for all objects requested by a user or for only some of the objects? Why?

As per the example presented in the textbook, the traffic intensity in a typical institutional network connection without Web caching implemented is around 1. This causes the delay in receiving requested objects to grow very large. A Web cache can reduce this delay by providing a certain hit rate that the network itself cannot provide. The textbook example uses a Web cache with a hit rate of 40 percent. The 40 percent of requests that the Web cache handles are then satisfied right away while only 60 percent of the total requests need to be completed by the origin servers. Therefore, the traffic intensity in the institutional network is reduced from 1 to .6, and when .8 is considered a small intensity, this is a huge improvement in performance. 

Given the above example, the Web cache only directly reduces the delay of a fraction of requested objects. This is because hit rates of caches typically only range from .2 to .7, which means a cache cannot cover all of the requests moving through the network.

# R14. Telnet into a Web server and send a multiline request message. Include in the request message the "If-modified-since:" header line to force a response message with the "304 Not Modified" status code.

I could not get a response message from a site that included the "Last-Modified:" header, but here is the book example:

 - GET /fruit/kiwi.gif HTTP/1.1
 - Host: www.exotiquecuisine.com
 - If-Modified-Since: Wed, 9 Sep 2015 09:23:24

The last line was based on a "Last-Modified" value of the same date as in the "If-Modified-Since" field.

# R26. In Section 2.7, the UDP server described needed only one socket, whereas the TCP server needed two sockets. Why? If the TCP server were to support n simultaneous connections, each from a different client host, how many sockets would the TCP server need?

The TCP server needs two sockets because the client and server need to establish a connection via a three-way handshake before data can be sent back and forth. For n simultaneous connections, a TCP server needs n+1 sockets: one welcoming socket to handle incoming hosts and one connection socket per host.
