# Week 2 Lab Report: Python Server

# Collaboration

I referenced the following links/materials to write my code:

 - https://stackoverflow.com/questions/3437059/does-python-have-a-string-contains-substring-method
 - https://stackoverflow.com/questions/42458475/sending-image-over-sockets-only-in-python-image-can-not-be-open
 - https://gaia.cs.umass.edu/kurose_ross/programming/Python_code_only/WebServer_programming_lab_only.pdf
 - Section 2.7.2 in the Kurose textbook

# Introduction

In this lab, I built an HTTP server in Python to gain a deeper understanding of how web servers process information and how GET requests/responses work. During this process, I learned that you have to read images as bytes and that you have to send regular file data through the server as a byte-encoded string. I also learned how to build a socket connection in Python and format HTTP requests.

# Process

To begin, I copied the starter code for a TCP server/client in Python from the Kurose textbook. The server worked right away when I ran it, but the client seemed to stall. Looking more closely at the textbook explanation of the code, I realized it was because I needed to insert my own IP for the connection to run on. After trying several of the IPs I have in my DNS configuration file, I thought I was out of luck. However, I remembered that I could do an IP lookup from my terminal, and the address I came up with allowed my client/server connection to finally work.

Once I had a server that could communicate with at least one host, I took it to the next level and attempted to process an HTTP GET request on the port I was listening to. This worked right away; I was pretty pleased with my intuition. I just took out the lines of code that converted a string sent by my Python client into uppercase and printed whatever message my server received. Upon navigating to localhost:12000/index.html while my server was running, the GET request from the site populated in my terminal.

The next part proved to be sort of tricky: I had to formulate a valid HTTP response to send over my server to be processed as HTML text on the other side. It took me a lot of trial and error to finally come up with syntax that worked. It turned out the secret was in the number of carriage returns I used; even if there isn't any information to post in every line of the header, there should still be return characters on each line. I also discovered that I had to specify content type in order for the HTML text I was sending over to be displayed properly.

The rest of the lab was fairly smooth sailing. I used the tester files from the course website to make sure I could serve files instead of hard-coding the HTML, and save for some syntax errors in the files that initially prevented some elements from working, everything functioned. The final hiccup I had was figuring out how to display pictures on my site. Thanks to some StackOverflow articles, I found out how to check for .JPG substrings and read an image as bytes in Python. I knew that should have worked right away in theory, but it took some time to figure out that my images had to be stored in a subdirectory called "img" in order for them to be properly grabbed by the website. I noticed this after seeing that the URL in the GET request included "/img/*picture*.JPG. Once I got that worked out, I had a fully functioning HTTP server.

# See the README file included in this folder for instructions on running the server (they are pretty straightforward).
