from socket import *

#set up socket
serverPort = 12000
serverSocket = socket(AF_INET, SOCK_STREAM)
serverSocket.bind(('',serverPort))
serverSocket.listen(1)
print('The server is ready to receive')

#receive/send information over the socket
while True:
    connectionSocket, addr = serverSocket.accept()
    try:
        request = connectionSocket.recv(1024).decode()
        print(request)
        filename = request.split()[1]
        response = 'HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n'
        connectionSocket.send(response.encode())

        #test for photo data vs. file data and
        #read accordingly
        if ".JPG" in filename:
            output = open(filename[1:], 'rb').read()
            connectionSocket.send(output)
        else:
            output = open(filename[1:]).read()
            connectionSocket.send(output.encode())

    #if a requested file is not found
    except IOError:
        response2 = 'HTTP/1.1 404 ERROR\r\n'

