# How to Run my Server

This server is coded in Python, so all you have to do is type the command "python3 TCPServer.py". It will then serve any files requested by the client as well as their dependencies. Include the files you would like sent to the client site in the same directory as the server program. If you are serving images, include those in a subdirectory called "img". The client uses that path to request any pictures coded in the HTML.

To stop the program, send "Ctrl+Z" in the terminal. The server will run continuously until this happens. If you get an "Address already in use" error when attempting to start the server, change the port number in the source code or restart your terminal.
