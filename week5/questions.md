# Week 5 Questions

## R17. Suppose two TCP connections are present over some bottleneck link of rate R  bps. Both connections have a huge file to send (in the same direction over the  bottleneck link). The transmissions of the files start at the same time. What  transmission rate would TCP like to give to each of the connections?

A fair transmission rate for each connection would be R bps/2; the overall transmission rate of the bottleneck link is split evenly between the two connections.

## R18. True or false? Consider congestion control in TCP. When the timer expires at  the sender, the value of ssthresh is set to one half of its previous value.

This is false; the value of ssthresh is set to one half of the value of cwnd, the congestion window, when the timer runs out.

## P27. Host A and B are communicating over a TCP connection, and Host B has  already received from A all bytes up through byte 126. Suppose Host A  then sends two segments to Host B back-to-back. The first and second segments contain 80 and 40 bytes of data, respectively. In the first segment,  the sequence number is 127, the source port number is 302, and the destination port number is 80. Host B sends an acknowledgment whenever it  receives a segment from Host A. 

#a. In the second segment sent from Host A to B, what are the sequence number, source port number, and destination port number?  

 - If the sequence number of the next segment is equal to the sequence number of the first segment plus the number of bytes in the first segment, the sequence number would be 207. The source and destination ports are still 302 and 80, respectively.

#b. If the first segment arrives before the second segment, in the acknowledgment of the first arriving segment, what is the acknowledgment number,  the source port number, and the destination port number?  

 - As long as the first segment arrives before the second, its ACK will have the same number as the second segment. Therefore, the acknowledgment number, source port number, and destination port number are 207, 80, and 302, respectively.

#c. If the second segment arrives before the first segment, in the acknowledgment of the first arriving segment, what is the acknowledgment number?  

 - If the second segment arrives before the first, the receiver will initially think the first packet was dropped, meaning the ACK for the second packet will have the same acknowledgment number as the sequence number for the first packet: 127.

#d. Suppose the two segments sent by A arrive in order at B. The first  acknowledgment is lost and the second acknowledgment arrives after the  first timeout interval. Draw a timing diagram, showing these segments  and all other segments and acknowledgments sent. (Assume there is no additional packet loss.) For each segment in your figure, provide the  sequence number and the number of bytes of data; for each acknowledgment that you add, provide the acknowledgment number.

 - My figure is included as a separate file.

## P33. In Section 3.5.3, we discussed TCP’s estimation of RTT. Why do you think  TCP avoids measuring the SampleRTT for retransmitted segments?

If the retransmitted packet ultimately did not result in a drop, and an ACK comes through for the original segment, the sender will use this ACK to calculate the SampleRTT for the retransmitted packet rather than the original packet, skewing the calculation.

## P36. In Section 3.5.4, we saw that TCP waits until it has received three duplicate ACKs before performing a fast retransmit. Why do you think the TCP  designers chose not to perform a fast retransmit after the first duplicate ACK  for a segment is received?

It is not uncommon for packets to arrive out-of-order from the sender rather than being dropped, so to avoid sending too many packets across the network and taking up unnecessary bandwidth, TCP does not peform fast retransmit after a single duplicate ACK.

## P40. Consider Figure 3.61. Assuming TCP Reno is the protocol experiencing the  behavior shown above, answer the following questions. In all cases, you  should provide a short discussion justifying your answer.  

#a. Identify the intervals of time when TCP slow start is operating.  

 - Slow start is operating from 1 to 6 and from 23 to 26. This can be determined by the exponential curve at each of those portions of the graph. 

#b. Identify the intervals of time when TCP congestion avoidance is operating.  

 - Congestion avoidance is happening from 6 to 16 and 17 to 22. The congestion window size is linearly increasing rather than exponentially increasing to indicate this.

#c. After the 16th transmission round, is segment loss detected by a triple  duplicate ACK or by a timeout?  

 - There was a triple duplicate ACK because the congestion window size did not decrease to 1. That would have occurred to indicate a timeout; TCP is instead entering fast recovery phase.

#d. After the 22nd transmission round, is segment loss detected by a triple  duplicate ACK or by a timeout?

 - This is a timeout because the congestion window size dropped all the way to 1 rather than only dropping a few sizes.

#e. What is the initial value of ssthresh at the first transmission round?  

 - The value of ssthresh should be around 32. This is when the first congestion avoidance phase begins during the transmission, and the beginning of congestion avoidance is triggered by the congestion window and the slow-start threshold becoming equal.

#f. What is the value of ssthresh at the 18th transmission round?  

 - The value of ssthresh should be 21 at this point. This is after the triple duplicat ACK event, so since the congestion window size was 42 when loss was detected, ssthresh would be half of 42, which is 21. 

#g. What is the value of ssthresh at the 24th transmission round?  

 - The value of ssthresh should be 14 here. This is after the timeout event, and the original congestion window size at the time of loss was 28.

#h. During what transmission round is the 70th segment sent?  

 - Adding up the number of packets sent through round seven by analyzing the congestion window size up until that point yields 96. There were 63 packets sent by the end of the 6th transmission round. Therefore, the 70th packet was sent during round 7.

#i. Assuming a packet loss is detected after the 26th round by the receipt of  a triple duplicate ACK, what will be the values of the congestion window  size and of ssthresh?  

 - The congestion window size at that round is 8, so both the congestion window size and ssthresh would drop to half of that value, which is 4.

#j. Suppose TCP Tahoe is used (instead of TCP Reno), and assume that triple  duplicate ACKs are received at the 16th round. What are the ssthresh  and the congestion window size at the 19th round?  

 - The congestion window would drop to 1 instead of picking back up at 24, and the value of ssthresh would still be 21.

#k. Again suppose TCP Tahoe is used, and there is a timeout event at  22nd round. How many packets have been sent out from 17th round till  22nd round, inclusive?

 - TCP would go into slow-start phase following the congestion window dropping to 1, so the number of packets received per round would be multiplied by 2 every time until hitting ssthresh. Therefore, the number of packets sent is 1+2+4+8+16+21 = 52.
