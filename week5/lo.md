# Week 5 Learning Objectives

# 1. I can explain the problem that TCP congestion control is solving for, and how it solves that problem.

The textbook outlines three main scenarios in which congestion becomes a problem that needs to be solved:

 - Two hosts have a connection with a single hop between source and destination, and there is a danger of overstepping bandwidth, causing infinite delay.

 - There is finite buffer space for a connection, meaning packets will be dropped upon arriving to an already-full buffer.

 - Two or more pairs of hosts are sending packets through a single router, and too much traffic from some hosts can cause denial of service to others.

TCP solves these problems through the many facets of congestion control. The sender has a variable called the "congestion window" that puts a number on how many unacknowledged packets the sender can send over the connection at a time. The sender uses the number of acknowledgments it receives or the lack thereof to determine whether or not congestion has occurred. This means TCP is a self-clocking protocol. If the sender receives three duplicate ACKs in a row, TCP does a fast retransmit and goes into fast recovery mode. This is where the congestion window increases by one for every duplicate ACK that was received and goes into congestion avoidance mode (where the congestion window continues increasing by one each cycle) until the transmission is complete or another loss event occurs. If a timeout happens rather than triple duplicate ACKs, the congestion window size will drop all the way to one, and TCP will go into a slow-start phase. The congestion window will increase exponentially each round until it hits the slow-start threshold, denoted by "ssthresh." When a loss event occurs, ssthresh is set to half the congestion window size.

There are three types of TCP noted in the text that do congestion control in different ways. The first is TCP Tahoe, which incorporates the slow-start phase and congestion control phase, but not the fast recovery phase. A newer version, TCP Reno, makes up for what Tahoe lacks and includes the recovery phase for triple duplicate ACKs. It also has an Additive Increase, Multiplicative Decrease method for loss events; it increases the congestion window size by one during each round of the congestion avoidance phase and cuts the window in half when triple duplicate ACKs are detected. Finally, there is TCP Cubic. This type creates a cubic function between the current time and the amount of time it would take to reach the maximum congestion window size before a certain loss event to calculate the window size, speeding up recovery.

Packet delay and the network itself can also signal congestion. Explicit Congestion Notification is a way that a router indicates congestion. The router will send a special bit to the receiver, which will notify the sender that congestion is occurring and loss is a risk. Another type of TCP, TCP Vegas, indicates congestion through packet delay. It keeps track of the ratio between the congestion window and the minimum RTT; if the current throughput is less than this number, congestion is probable.

A final concept relating to congestion control is fairness. BBR congestion control protocol implements this idea. If two or more hosts are connected to a single bottleneck link, fairness allows each one to share an equal portion of the bandwidth. If packet loss occurs, that portion decreases slightly for each host involved, maintaining the ratio of bandwidth between them.

# 2. I can explain the problem that TCP flow control is solving for, and how it solves that problem.

TCP flow control attempts to solve the problem of the sender overflowing the receiver's buffer. To do this, it equalizes the sender's rate with the receiver's rate. The receiver has a variable called the receive window that keeps track of how many packets it can accept at a time and is the amount of spare room in the buffer. The number of unacknowledged packets is kept below the size of the receive window, ensuring that overflow does not occur. To keep the value of the receive window from hitting zero, when the buffer is full, the sender sends one-byte segments to the receiver that are not enough to cause overflow but are enough to continue stuffing the buffer until it begins to empty.
